package dilawarkhan.azeemi.talentuser.Api;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.MultiplePulse;
import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;

import dilawarkhan.azeemi.talentuser.R;
import okhttp3.MediaType;
import okhttp3.RequestBody;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class Utils
{
    public static void warning_dialog(final Activity activity, String title, String message)
    {
        new FancyAlertDialog.Builder(activity)
                .setTitle(title)
                .setBackgroundColor(Color.parseColor("#f39c12"))  //Don't pass R.color.colorvalue
                .setMessage(message)
                .setNegativeBtnText("Cancel")
                .setPositiveBtnBackground(Color.parseColor("#f39c12"))  //Don't pass R.color.colorvalue
                .setPositiveBtnText("Ok")
                .setNegativeBtnBackground(Color.parseColor("#FFA9A7A8"))  //Don't pass R.color.colorvalue
                .setAnimation(Animation.SIDE)
                .isCancellable(false)
                .setIcon(R.drawable.warning, Icon.Visible)
                .OnPositiveClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {

                    }
                })
                .OnNegativeClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {

                    }
                })
                .build();
    }

    public static void success_dialog(final Activity activity, String title, String message)
    {
        new FancyAlertDialog.Builder(activity)
                .setTitle(title)
                .setBackgroundColor(Color.parseColor("#2ecc71"))  //Don't pass R.color.colorvalue
                .setMessage(message)
                .setNegativeBtnText("Cancel")
                .setPositiveBtnBackground(Color.parseColor("#2ecc71"))  //Don't pass R.color.colorvalue
                .setPositiveBtnText("Ok")
                .setNegativeBtnBackground(Color.parseColor("#FFA9A7A8"))  //Don't pass R.color.colorvalue
                .setAnimation(Animation.SIDE)
                .isCancellable(false)
                .setIcon(R.drawable.success, Icon.Visible)
                .OnPositiveClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {

                    }
                })
                .OnNegativeClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {

                    }
                })
                .build();
    }

    public static void error_dialog(final Activity activity, String title, String message)
    {
        new FancyAlertDialog.Builder(activity)
                .setTitle(title)
                .setBackgroundColor(Color.parseColor("#e74c3c"))  //Don't pass R.color.colorvalue
                .setMessage(message)
                .setNegativeBtnText("Cancel")
                .setPositiveBtnBackground(Color.parseColor("#e74c3c"))  //Don't pass R.color.colorvalue
                .setPositiveBtnText("Ok")
                .setNegativeBtnBackground(Color.parseColor("#FFA9A7A8"))  //Don't pass R.color.colorvalue
                .setAnimation(Animation.SIDE)
                .isCancellable(false)
                .setIcon(R.drawable.delete, Icon.Visible)
                .OnPositiveClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {

                    }
                })
                .OnNegativeClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {

                    }
                })
                .build();
    }

    public static void enableLayout(Context context , android.view.animation.Animation animation, RelativeLayout layout, ProgressBar progressBar)
    {
        animation = AnimationUtils.loadAnimation(context, R.anim.fade_enter);
        layout.setAnimation(animation);
        progressBar.setVisibility(View.GONE);
    }

    public static void disable_layout(Context context , android.view.animation.Animation animation, RelativeLayout layout, ProgressBar progressBar)
    {
        animation = AnimationUtils.loadAnimation(context, R.anim.fade_exit);
        layout.setAnimation(animation);
        progressBar.setVisibility(View.VISIBLE);
        Sprite doubleBounce = new MultiplePulse();
        progressBar.setIndeterminateDrawable(doubleBounce);
    }

    public static void EnableProgressBar(ProgressBar progressBar)
    {
        progressBar.setVisibility(View.VISIBLE);
        Sprite doubleBounce = new MultiplePulse();
        progressBar.setIndeterminateDrawable(doubleBounce);
    }

    public static boolean isOnline(Context ctx) {

        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        else {
            return false;
        }
    }

    public static RequestBody getSimpleTextBody(String param)
    {
        return RequestBody.create(MediaType.parse("text/plain"),param);
    }
}