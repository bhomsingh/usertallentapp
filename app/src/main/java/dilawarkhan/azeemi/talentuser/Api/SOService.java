package dilawarkhan.azeemi.talentuser.Api;

import java.util.List;

import dilawarkhan.azeemi.talentuser.Classes.CategoryClas;
import dilawarkhan.azeemi.talentuser.Classes.CategoryForSpinnerClass;
import dilawarkhan.azeemi.talentuser.Classes.LiveStreamResultClass;
import dilawarkhan.azeemi.talentuser.Classes.LoginClass;
import dilawarkhan.azeemi.talentuser.Classes.MyBookShowDetailClass;
import dilawarkhan.azeemi.talentuser.Classes.MyBookingListClass;
import dilawarkhan.azeemi.talentuser.Classes.ResponseClass;
import dilawarkhan.azeemi.talentuser.Classes.ShowClass;
import dilawarkhan.azeemi.talentuser.Classes.ShowCommentClass;
import dilawarkhan.azeemi.talentuser.Classes.ShowDetailClass;
import dilawarkhan.azeemi.talentuser.Classes.SubCategoryForSpinnerClass;
import dilawarkhan.azeemi.talentuser.Classes.Sub_cateegoryClass;
import dilawarkhan.azeemi.talentuser.Classes.UserDetailClass;
import dilawarkhan.azeemi.talentuser.Classes.UserRegistration;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

public interface SOService {
// @GET("/answers?order=desc&sort=activity&site=stackoverflow")
// Call<SOAnswersResponse> getAnswers();

    @Multipart
    @POST
    Call<UserRegistration> getUserRegistrationResponse(@Url String url
            , @Part("req_key") RequestBody reqkey
            , @Part("name") RequestBody name
            , @Part("email") RequestBody email
            , @Part("password") RequestBody password
            , @Part("phone") RequestBody phone
            , @Part("gender") RequestBody gender
            , @Part MultipartBody.Part image);

    @Multipart
    @POST
    Call<LoginClass> getLoginResponse(@Url String url
            , @Part("req_key") RequestBody reqkey
            , @Part("email") RequestBody email
            , @Part("password") RequestBody password);

    @Multipart
    @POST
    Call<List<CategoryClas>> getCategoryResponse(@Url String url
            , @Part("req_key") RequestBody reqkey
    );

    @Multipart
    @POST
    Call<List<ShowClass>> getShowResponse(@Url String url
            , @Part("req_key") RequestBody reqkey
    );

    @Multipart
    @POST
    Call<UserDetailClass> getUserDetailResponse(@Url String url
            , @Part("req_key") RequestBody reqkey
            , @Part("u_id") RequestBody u_id);

    @Multipart
    @POST
    Call<ShowDetailClass> getShowDetailResponse(@Url String url
            , @Part("req_key") RequestBody reqkey
            , @Part("show_id") RequestBody show_id);

    @Multipart
    @POST
    Call<ResponseClass> getBookingResponse(@Url String url
            , @Part("req_key") RequestBody reqkey
            , @Part("show_id") RequestBody show_id
            , @Part("user_id") RequestBody user_id
            , @Part("fee") RequestBody fee
    );

    @Multipart
    @POST
    Call<List<MyBookingListClass>> getMyBookingResponse(@Url String url
            , @Part("req_key") RequestBody reqkey
            , @Part("user_id") RequestBody user_id
    );

    @Multipart
    @POST
    Call<MyBookShowDetailClass> getMyBookShowDetailResponse(@Url String url
            , @Part("req_key") RequestBody reqkey
            , @Part("show_id") RequestBody show_id);

    @Multipart
    @POST
    Call<LiveStreamResultClass> getLiveResponse(@Url String url
            , @Part("req_key") RequestBody reqkey
            , @Part("show_id") RequestBody show_id);

    @Multipart
    @POST
    Call<List<ShowCommentClass>> getShowCommentResponse(@Url String url
            , @Part("req_key") RequestBody reqkey
            , @Part("show_id") RequestBody show_id
    );

    @Multipart
    @POST
    Call<ShowCommentClass> getRealTimeCommentResponse(@Url String url
            , @Part("req_key") RequestBody reqkey
            , @Part("show_id") RequestBody show_id
            , @Part("user_id") RequestBody user_id
            , @Part("comment") RequestBody comment
    );

    @Multipart
    @POST
    Call<List<CategoryClas>> getCategoryListResponse(@Url String url
            , @Part("req_key") RequestBody reqkey
            , @Part("limit") RequestBody limit
    );

    @Multipart
    @POST
    Call<List<ShowClass>> getShowFeaturedListResponse(@Url String url
            , @Part("req_key") RequestBody reqkey
            , @Part("limit") RequestBody limit);


    @Multipart
    @POST
    Call<List<ShowClass>> getShowListResponse(@Url String url
            , @Part("req_key") RequestBody reqkey
            , @Part("limit") RequestBody limit);



    @Multipart
    @POST
    Call<UserDetailClass> getProfileResponse(@Url String url
            , @Part("req_key") RequestBody reqkey
            , @Part("u_id") RequestBody user_id);


    // Update profile
    @Multipart
    @POST
    Call<ResponseClass> getProfileUpdateResponse(@Url String url
            , @Part("req_key") RequestBody reqkey
            , @Part("name") RequestBody name
            , @Part("email") RequestBody email
            , @Part("pass") RequestBody pass
            , @Part("dob") RequestBody dob
            , @Part("gender") RequestBody gender
            , @Part("phone") RequestBody phone
            , @Part("address") RequestBody address
            , @Part("id") RequestBody id
            , @Part("interest") RequestBody interest
            , @Part MultipartBody.Part pro_image);

    @Multipart
    @POST
    Call<List<Sub_cateegoryClass>> getSub_catResponse(@Url String url
            , @Part("req_key") RequestBody reqkey
            , @Part("cat_id") RequestBody cat_id
            , @Part("limit") RequestBody limit);


    @Multipart
    @POST
    Call<List<ShowClass>> getshowsubCategoryResponse(@Url String url
            , @Part("req_key") RequestBody reqkey
            , @Part("cat_id") RequestBody cat_id
            , @Part("sub_cat_id") RequestBody sub_cat_id
            , @Part("limit") RequestBody limit);

    @Multipart
    @POST
    Call<List<CategoryForSpinnerClass>> getCategoryListForSpinnerResponse(@Url String url
            , @Part("req_key") RequestBody reqkey);

    @Multipart
    @POST
    Call<List<SubCategoryForSpinnerClass>> getSubCategoryListForSpinnerResponse(@Url String url
            , @Part("req_key") RequestBody reqkey
            , @Part("cat_id") RequestBody cat_id);


    @Multipart
    @POST
    Call<List<ShowClass>> getSearchFilterListForSpinnerResponse(@Url String url
            , @Part("req_key") RequestBody reqkey
            , @Part("cat_id") RequestBody cat_id
            , @Part("sub_cat_id") RequestBody sub_cat_id
            , @Part("keyword") RequestBody keyword
            , @Part("featured") RequestBody featured);



}