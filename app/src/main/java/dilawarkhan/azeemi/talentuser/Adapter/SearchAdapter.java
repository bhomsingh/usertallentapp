package dilawarkhan.azeemi.talentuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import dilawarkhan.azeemi.talentuser.Activity.ShowDetailActivity;
import dilawarkhan.azeemi.talentuser.Classes.Animation;
import dilawarkhan.azeemi.talentuser.Classes.ShowClass;
import dilawarkhan.azeemi.talentuser.R;

public class SearchAdapter  extends RecyclerView.Adapter<SearchAdapter.ShowViewHolder>
    {
        private Context context;
        List<ShowClass> showClassList;

    public SearchAdapter(Context context, List<ShowClass> showClassList) {

        this.context = context;
        this.showClassList = showClassList;
    }

        @Override
        public SearchAdapter.ShowViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            LayoutInflater inflater = LayoutInflater.from(context);
            View view = inflater.inflate(R.layout.card_search, null);
            return new SearchAdapter.ShowViewHolder(view);
        }

        @Override
        public int getItemCount()
        {
            return showClassList.size();
        }

        class ShowViewHolder extends RecyclerView.ViewHolder
        {
            CardView cardView;
            ImageView banner_image, featured_logo;
            TextView tv_tittle, tv_Desc;
            public ShowViewHolder(View itemView)
            {
                super(itemView);
                cardView = itemView.findViewById(R.id.card_view);
                banner_image = itemView.findViewById(R.id.IV_banner_image);
                featured_logo = itemView.findViewById(R.id.IV_featured);
                tv_tittle = itemView.findViewById(R.id.TV_title);
                tv_Desc = itemView.findViewById(R.id.TV_description);
            }
        }


        @Override
        public void onBindViewHolder(@NonNull ShowViewHolder holder, int position) {


            final ShowClass showClass = showClassList.get(position);
            if(showClass.getShowFeatured() != null && showClass.getShowFeatured().equals("1"))
            {
                holder.featured_logo.setVisibility(View.VISIBLE);
            }
            Glide.with(context).load(showClass.getShowBannerImage()).into(holder.banner_image);
            holder.tv_tittle.setText(showClass.getShowTitle());
            holder.tv_Desc.setText(showClass.getShowShortDescription());
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, ShowDetailActivity.class);
                    i.putExtra("show_id",showClass.getShowId());
                    context.startActivity(i);
                    Animation.fade(context);
                }
            });

        }
    }
