package dilawarkhan.azeemi.talentuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import dilawarkhan.azeemi.talentuser.Activity.MyBookingShowDetailActivity;
import dilawarkhan.azeemi.talentuser.Classes.Animation;
import dilawarkhan.azeemi.talentuser.Classes.MyBookingListClass;
import dilawarkhan.azeemi.talentuser.R;
import es.dmoral.toasty.Toasty;

public class MyBookingListAdapter extends RecyclerView.Adapter<MyBookingListAdapter.MyBookingViewHolder>
{
    private Context context;
    List<MyBookingListClass> myBookingListClassList;

    public MyBookingListAdapter(Context context, List<MyBookingListClass> myBookingListClassList) {
        this.context = context;
        this.myBookingListClassList = myBookingListClassList;
    }

    @Override
    public MyBookingViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.card_my_booking, null);
        return new MyBookingViewHolder(view);
    }

    @Override
    public int getItemCount()
    {
        return myBookingListClassList.size();
    }

    class MyBookingViewHolder extends RecyclerView.ViewHolder
    {
        CardView cardView;
        ImageView banner_image, featured_logo;
        TextView tv_tittle, tv_Desc;
        public MyBookingViewHolder(View itemView)
        {
            super(itemView);
            cardView = itemView.findViewById(R.id.card_view);
            banner_image = itemView.findViewById(R.id.IV_banner_image);
            featured_logo = itemView.findViewById(R.id.IV_featured);
            tv_tittle = itemView.findViewById(R.id.TV_title);
            tv_Desc = itemView.findViewById(R.id.TV_description);
        }
    }

    @Override
    public void onBindViewHolder(MyBookingViewHolder holder, int position)
    {
        final MyBookingListClass myBookingListClass = myBookingListClassList.get(position);
        if(myBookingListClass.getShowFeatured() != null && myBookingListClass.getShowFeatured().equals("1"))
        {
            holder.featured_logo.setVisibility(View.VISIBLE);
        }
        Glide.with(context).load(myBookingListClass.getShowBannerImage()).into(holder.banner_image);
        holder.tv_tittle.setText(myBookingListClass.getShowTitle());
        holder.tv_Desc.setText(myBookingListClass.getShowShortDescription());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, MyBookingShowDetailActivity.class);
                i.putExtra("show_id",myBookingListClass.getShowId());
                context.startActivity(i);
                Animation.fade(context);
                //Toasty.success(context,myBookingListClass.getShowId(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
