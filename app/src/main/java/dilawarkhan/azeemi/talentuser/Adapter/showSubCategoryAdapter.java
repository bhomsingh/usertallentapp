package dilawarkhan.azeemi.talentuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import dilawarkhan.azeemi.talentuser.Activity.ShowDetailActivity;
import dilawarkhan.azeemi.talentuser.Activity.showSubCategoryActivity;
import dilawarkhan.azeemi.talentuser.Classes.Animation;
import dilawarkhan.azeemi.talentuser.Classes.ShowClass;
import dilawarkhan.azeemi.talentuser.R;
import es.dmoral.toasty.Toasty;

public class showSubCategoryAdapter extends RecyclerView.Adapter<showSubCategoryAdapter.ShowOrginalViewHolder>{

        private Context context;
        List<ShowClass> showSubCatClasLists;

    public showSubCategoryAdapter(showSubCategoryActivity context, List<ShowClass> showsCategory_subCategoryClass) {

        this.context = context;
        this.showSubCatClasLists = showsCategory_subCategoryClass;

    }

        @NonNull
        @Override
        public showSubCategoryAdapter.ShowOrginalViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.show_subcategory, null);
        return new showSubCategoryAdapter.ShowOrginalViewHolder(view);

        }

        @Override
        public void onBindViewHolder(@NonNull ShowOrginalViewHolder holder, int position) {

            final ShowClass showClass = showSubCatClasLists.get(position);
            if(showClass.getShowFeatured() != null && showClass.getShowFeatured().equals("1"))
            {
                holder.featured_logo.setVisibility(View.VISIBLE);
            }
            Glide.with(context).load(showClass.getShowBannerImage()).into(holder.banner_image);
            holder.tv_tittle.setText(showClass.getShowTitle());
            holder.tv_Desc.setText(showClass.getShowShortDescription());

            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i = new Intent(context, ShowDetailActivity.class);
                    i.putExtra("show_id",showClass.getShowId());
                    context.startActivity(i);
                    Animation.fade(context);

                }
            });
     }

@Override
public int getItemCount() {
        return showSubCatClasLists.size();
        }


public class ShowOrginalViewHolder extends RecyclerView.ViewHolder {

    CardView cardView;
    ImageView banner_image, featured_logo;
    TextView tv_tittle, tv_Desc;

    public ShowOrginalViewHolder(@NonNull View itemView) {
        super(itemView);

        cardView = itemView.findViewById(R.id.card_view);
        banner_image = itemView.findViewById(R.id.IV_banner_image);
        featured_logo = itemView.findViewById(R.id.IV_featured);
        tv_tittle = itemView.findViewById(R.id.TV_title);
        tv_Desc = itemView.findViewById(R.id.TV_description);

    }
}

    public void addNewData(List<ShowClass> showClasList)
    {
        showSubCatClasLists.addAll(showClasList);
        notifyDataSetChanged();
    }
}
