package dilawarkhan.azeemi.talentuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import dilawarkhan.azeemi.talentuser.Activity.showSubCategoryActivity;
import dilawarkhan.azeemi.talentuser.Classes.Animation;
import dilawarkhan.azeemi.talentuser.Classes.Sub_cateegoryClass;
import dilawarkhan.azeemi.talentuser.R;
import es.dmoral.toasty.Toasty;

public class Sub_categoryOrignalAdapter extends RecyclerView.Adapter<Sub_categoryOrignalAdapter.ShowOrginalViewHolder> {

    private Context context;
    List<Sub_cateegoryClass> sub_catClasLists;

    public Sub_categoryOrignalAdapter(Context context, List<Sub_cateegoryClass> sub_cat_ClasLists) {
        this.context = context;
        this.sub_catClasLists = sub_cat_ClasLists;
    }

    @NonNull
    @Override
    public Sub_categoryOrignalAdapter.ShowOrginalViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.sub_category, null);
        return new Sub_categoryOrignalAdapter.ShowOrginalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShowOrginalViewHolder holder, int position) {

        final Sub_cateegoryClass sub_catClass = sub_catClasLists.get(position);

        Glide.with(context).load(sub_catClass.getSubcategoryImage()).into(holder.banner_image);

        holder.tv_tittle.setText(sub_catClass.getSubcategoryName());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(context, showSubCategoryActivity.class);
                i.putExtra("cat_id",sub_catClass.getCategoryId());
                i.putExtra("sub_cat_id",sub_catClass.getSubcategoryId());
                context.startActivity(i);
                Animation.fade(context);

            }
        });
    }

    @Override
    public int getItemCount() {
        return sub_catClasLists.size();
    }


    public class ShowOrginalViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        ImageView banner_image;
        TextView tv_tittle;

        public ShowOrginalViewHolder(@NonNull View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.card_view);
            banner_image = itemView.findViewById(R.id.IV_banner_image);
            tv_tittle = itemView.findViewById(R.id.TV_title);

        }
    }

    public void addNewData(List<Sub_cateegoryClass> catClasList)
    {
        sub_catClasLists.addAll(catClasList);
        notifyDataSetChanged();
    }
}
