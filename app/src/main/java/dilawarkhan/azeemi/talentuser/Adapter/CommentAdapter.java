package dilawarkhan.azeemi.talentuser.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import dilawarkhan.azeemi.talentuser.Classes.ShowCommentClass;
import dilawarkhan.azeemi.talentuser.R;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ShowCommentViewHolder>
{
    private Context context;
    private List<ShowCommentClass> showCommentClassList;

    public CommentAdapter(Context context, List<ShowCommentClass> showCommentClassList) {
        this.context = context;
        this.showCommentClassList = showCommentClassList;
    }

    @Override
    public ShowCommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.card_comment, null);
        return new ShowCommentViewHolder(view);
    }

    @Override
    public int getItemCount()
    {
        return showCommentClassList.size();
    }

    class ShowCommentViewHolder extends RecyclerView.ViewHolder
    {
        ImageView user_image;
        TextView tv_name, tv_comment, tv_comment_date;
        public ShowCommentViewHolder(View itemView)
        {
            super(itemView);
            user_image = (ImageView) itemView.findViewById(R.id.img_profile);
            tv_name = (TextView) itemView.findViewById(R.id.user_name);
            tv_comment = (TextView) itemView.findViewById(R.id.message);
            tv_comment_date = (TextView) itemView.findViewById(R.id.timestamp);
        }
    }

    @Override
    public void onBindViewHolder(ShowCommentViewHolder holder, int position)
    {
        final ShowCommentClass showCommentClass = showCommentClassList.get(position);
        Glide.with(context).load(showCommentClass.getUserImg()).into(holder.user_image);

        holder.tv_name.setText(showCommentClass.getUserName());
        holder.tv_comment.setText(showCommentClass.getComment());
        holder.tv_comment_date.setText(showCommentClass.getCommentTime());
    }

    public void addSingleComment(ShowCommentClass showCommentClass)
    {
        showCommentClassList.add(showCommentClass);
        notifyDataSetChanged();
    }

    public void updateComments(List<ShowCommentClass> commentsList)
    {
        showCommentClassList = commentsList;
        notifyDataSetChanged();
    }
}
