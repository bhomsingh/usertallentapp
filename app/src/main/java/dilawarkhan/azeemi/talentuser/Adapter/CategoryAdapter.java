package dilawarkhan.azeemi.talentuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import dilawarkhan.azeemi.talentuser.Activity.ShowActivity;
import dilawarkhan.azeemi.talentuser.Activity.sub_category_Activity;
import dilawarkhan.azeemi.talentuser.Activity.showSubCategoryActivity;
import dilawarkhan.azeemi.talentuser.Classes.Animation;
import dilawarkhan.azeemi.talentuser.Classes.CategoryClas;
import dilawarkhan.azeemi.talentuser.R;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>
{
    private Context context;
    List<CategoryClas> categoryClasLists;

    public CategoryAdapter(Context context, List<CategoryClas> categoryClasLists)
    {
        this.context = context;
        this.categoryClasLists = categoryClasLists;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.card_category, null);
        return new CategoryViewHolder(view);
    }

    @Override
    public int getItemCount()
    {
        return categoryClasLists.size();
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder
    {
        CardView cardView;
        ImageView banner_image;
        TextView tv_tittle;
        public CategoryViewHolder(View itemView)
        {
            super(itemView);
            cardView = itemView.findViewById(R.id.card_view);
            banner_image = itemView.findViewById(R.id.IV_banner_image);
            tv_tittle = itemView.findViewById(R.id.TV_title);
        }
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position)
    {
        final CategoryClas categoryClas = categoryClasLists.get(position);
        Glide.with(context).load(categoryClas.getCategoryImage()).into(holder.banner_image);
        holder.tv_tittle.setText(categoryClas.getCategoryName());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            if(categoryClas.getSubCategory())
            {
                Intent i = new Intent(context, sub_category_Activity.class);
                i.putExtra("cat_id",categoryClas.getCategoryId());
                context.startActivity(i);
                Animation.fade(context);

            }
            else
            {
                Intent i = new Intent(context, showSubCategoryActivity.class);
                i.putExtra("cat_id",categoryClas.getCategoryId());
                i.putExtra("sub_cat_id","0");
                context.startActivity(i);
                Animation.fade(context);
            }
            }
        });
    }
}
