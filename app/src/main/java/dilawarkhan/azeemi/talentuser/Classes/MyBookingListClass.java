package dilawarkhan.azeemi.talentuser.Classes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyBookingListClass {

@SerializedName("success")
@Expose
private Boolean success;
@SerializedName("message")
@Expose
private String message;
@SerializedName("show_id")
@Expose
private String showId;
@SerializedName("show_featured")
@Expose
private String showFeatured;
@SerializedName("show_title")
@Expose
private String showTitle;
@SerializedName("show_short_description")
@Expose
private String showShortDescription;
@SerializedName("show_banner_image")
@Expose
private String showBannerImage;

public Boolean getSuccess() {
return success;
}

public void setSuccess(Boolean success) {
this.success = success;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public String getShowId() {
return showId;
}

public void setShowId(String showId) {
this.showId = showId;
}

public String getShowFeatured() {
return showFeatured;
}

public void setShowFeatured(String showFeatured) {
this.showFeatured = showFeatured;
}

public String getShowTitle() {
return showTitle;
}

public void setShowTitle(String showTitle) {
this.showTitle = showTitle;
}

public String getShowShortDescription() {
return showShortDescription;
}

public void setShowShortDescription(String showShortDescription) {
this.showShortDescription = showShortDescription;
}

public String getShowBannerImage() {
return showBannerImage;
}

public void setShowBannerImage(String showBannerImage) {
this.showBannerImage = showBannerImage;
}

}