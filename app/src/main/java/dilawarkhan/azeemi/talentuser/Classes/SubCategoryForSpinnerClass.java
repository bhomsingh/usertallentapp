package dilawarkhan.azeemi.talentuser.Classes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubCategoryForSpinnerClass {

@SerializedName("success")
@Expose
private Boolean success;
@SerializedName("message")
@Expose
private String message;
@SerializedName("subcategory_id")
@Expose
private String subcategoryId;
@SerializedName("subcategory_name")
@Expose
private String subcategoryName;

public Boolean getSuccess() {
return success;
}

public void setSuccess(Boolean success) {
this.success = success;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public String getSubcategoryId() {
return subcategoryId;
}

public void setSubcategoryId(String subcategoryId) {
this.subcategoryId = subcategoryId;
}

public String getSubcategoryName() {
return subcategoryName;
}

public void setSubcategoryName(String subcategoryName) {
this.subcategoryName = subcategoryName;
}

}