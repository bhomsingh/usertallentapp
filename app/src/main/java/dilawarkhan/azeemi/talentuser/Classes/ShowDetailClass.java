package dilawarkhan.azeemi.talentuser.Classes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShowDetailClass {

@SerializedName("success")
@Expose
private Boolean success;
@SerializedName("message")
@Expose
private String message;
@SerializedName("show_title")
@Expose
private String showTitle;
@SerializedName("show_short_description")
@Expose
private String showShortDescription;
@SerializedName("show_description")
@Expose
private String showDescription;
@SerializedName("show_banner_image")
@Expose
private String showBannerImage;
@SerializedName("show_date")
@Expose
private String showDate;
@SerializedName("show_start_time")
@Expose
private String showStartTime;
@SerializedName("show_end_time")
@Expose
private String showEndTime;
@SerializedName("show_fee")
@Expose
private String showFee;
@SerializedName("show_featured")
@Expose
private String showFeatured;
@SerializedName("user_count")
@Expose
private Integer userCount;

public Boolean getSuccess() {
return success;
}

public void setSuccess(Boolean success) {
this.success = success;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public String getShowTitle() {
return showTitle;
}

public void setShowTitle(String showTitle) {
this.showTitle = showTitle;
}

public String getShowShortDescription() {
return showShortDescription;
}

public void setShowShortDescription(String showShortDescription) {
this.showShortDescription = showShortDescription;
}

public String getShowDescription() {
return showDescription;
}

public void setShowDescription(String showDescription) {
this.showDescription = showDescription;
}

public String getShowBannerImage() {
return showBannerImage;
}

public void setShowBannerImage(String showBannerImage) {
this.showBannerImage = showBannerImage;
}

public String getShowDate() {
return showDate;
}

public void setShowDate(String showDate) {
this.showDate = showDate;
}

public String getShowStartTime() {
return showStartTime;
}

public void setShowStartTime(String showStartTime) {
this.showStartTime = showStartTime;
}

public String getShowEndTime() {
return showEndTime;
}

public void setShowEndTime(String showEndTime) {
this.showEndTime = showEndTime;
}

public String getShowFee() {
return showFee;
}

public void setShowFee(String showFee) {
this.showFee = showFee;
}

public String getShowFeatured() {
return showFeatured;
}

public void setShowFeatured(String showFeatured) {
this.showFeatured = showFeatured;
}

public Integer getUserCount() {
return userCount;
}

public void setUserCount(Integer userCount) {
this.userCount = userCount;
}

}