package dilawarkhan.azeemi.talentuser.Classes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShowCommentClass {

@SerializedName("success")
@Expose
private Boolean success;
@SerializedName("message")
@Expose
private String message;
@SerializedName("show_id")
@Expose
private String showId;
@SerializedName("user_name")
@Expose
private String userName;
@SerializedName("user_img")
@Expose
private String userImg;
@SerializedName("comment")
@Expose
private String comment;
@SerializedName("comment_time")
@Expose
private String commentTime;

public Boolean getSuccess() {
return success;
}

public void setSuccess(Boolean success) {
this.success = success;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public String getShowId() {
return showId;
}

public void setShowId(String showId) {
this.showId = showId;
}

public String getUserName() {
return userName;
}

public void setUserName(String userName) {
this.userName = userName;
}

public String getUserImg() {
return userImg;
}

public void setUserImg(String userImg) {
this.userImg = userImg;
}

public String getComment() {
return comment;
}

public void setComment(String comment) {
this.comment = comment;
}

public String getCommentTime() {
return commentTime;
}

public void setCommentTime(String commentTime) {
this.commentTime = commentTime;
}

}