package dilawarkhan.azeemi.talentuser.Classes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDetailClass {

@SerializedName("user_id")
@Expose
private String userId;
@SerializedName("user_name")
@Expose
private String userName;
@SerializedName("user_email")
@Expose
private String userEmail;
@SerializedName("user_password")
@Expose
private String userPassword;
@SerializedName("user_image")
@Expose
private String userImage;
@SerializedName("user_dob")
@Expose
private String userDob;
@SerializedName("user_gender")
@Expose
private String userGender;
@SerializedName("user_phone")
@Expose
private String userPhone;
@SerializedName("user_address")
@Expose
private String userAddress;

public String getUserId() {
return userId;
}

public void setUserId(String userId) {
this.userId = userId;
}

public String getUserName() {
return userName;
}

public void setUserName(String userName) {
this.userName = userName;
}

public String getUserEmail() {
return userEmail;
}

public void setUserEmail(String userEmail) {
this.userEmail = userEmail;
}

public String getUserPassword() {
return userPassword;
}

public void setUserPassword(String userPassword) {
this.userPassword = userPassword;
}

public String getUserImage() {
return userImage;
}

public void setUserImage(String userImage) {
this.userImage = userImage;
}

public String getUserDob() {
return userDob;
}

public void setUserDob(String userDob) {
this.userDob = userDob;
}

public String getUserGender() {
return userGender;
}

public void setUserGender(String userGender) {
this.userGender = userGender;
}

public String getUserPhone() {
return userPhone;
}

public void setUserPhone(String userPhone) {
this.userPhone = userPhone;
}

public String getUserAddress() {
return userAddress;
}

public void setUserAddress(String userAddress) {
this.userAddress = userAddress;
}

}