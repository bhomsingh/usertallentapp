package dilawarkhan.azeemi.talentuser.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.List;

import dilawarkhan.azeemi.talentuser.Adapter.CategoryAdapter;
import dilawarkhan.azeemi.talentuser.Adapter.ShowAdapter;
import dilawarkhan.azeemi.talentuser.Api.ApiUtils;
import dilawarkhan.azeemi.talentuser.Api.SOService;
import dilawarkhan.azeemi.talentuser.Api.Utils;
import dilawarkhan.azeemi.talentuser.Classes.Animation;
import dilawarkhan.azeemi.talentuser.Classes.CategoryClas;
import dilawarkhan.azeemi.talentuser.Classes.ShowClass;
import dilawarkhan.azeemi.talentuser.Classes.UserDetailClass;
import dilawarkhan.azeemi.talentuser.R;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //Test
    private RelativeLayout layout;
    private ProgressBar progressBar;
    private android.view.animation.Animation animation;
    private RecyclerView recycler_view_category, recycler_view_fshow, recycler_view_show;
    private List<CategoryClas> categoryClasList;
    private CategoryAdapter categoryAdapter;
    private List<ShowClass> showClassList;
    private ShowAdapter showAdapter;
    private String user_id;
    private TextView tv_uname, tv_uEmail, tv_view_all_category, tv_view_all_fshow, tv_view_all_show;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        user_id = Prefs.getString("user_id","0");

        layout       = findViewById(R.id.freez_layout);
        progressBar  = findViewById(R.id.progress);
        recycler_view_category  = findViewById(R.id.recycler_view_category);
        recycler_view_fshow  = findViewById(R.id.recycler_view_fshow);
        recycler_view_show  = findViewById(R.id.recycler_view_show);
        tv_view_all_category  = findViewById(R.id.tv_view_all_category);
        tv_view_all_fshow      = findViewById(R.id.tv_view_all_fshow);
        tv_view_all_show       = findViewById(R.id.tv_view_all_show);

        if(Utils.isOnline(HomeActivity.this))
        {
            Utils.disable_layout(HomeActivity.this,animation,layout,progressBar);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run()
                {
                    try
                    {
                        load_category();
                        load_featured_Show();
                        load_Shows();
                        getUserInfo();
                    }
                    catch (Exception e)
                    {
                        Toasty.error(HomeActivity.this,e.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                }
            }, 2000);
        }
        else
        {
            Utils.error_dialog(HomeActivity.this,"Internet Error","Internet Not Found");
        }

        tv_view_all_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, CategoryActivity.class));
                Animation.slideDown(HomeActivity.this);
            }
        });


        tv_view_all_fshow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, ShowFeturedActivity.class));
                Animation.slideDown(HomeActivity.this);
            }
        });


        tv_view_all_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this,ShowActivity.class));
                Animation.slideDown(HomeActivity.this);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        tv_uname = headerView.findViewById(R.id.tv_uname);
        tv_uEmail = headerView.findViewById(R.id.tv_uEmail);
        imageView = headerView.findViewById(R.id.imageView);

    }

    private void load_category()
    {
        SOService soService = ApiUtils.getSOService();
        soService.getCategoryResponse(ApiUtils.BASE_URL, Utils.getSimpleTextBody("load_category")).enqueue(new Callback<List<CategoryClas>>() {
            @Override
            public void onResponse(Call<List<CategoryClas>> call, Response<List<CategoryClas>> response) {
                if(response.isSuccessful())
                {
                    Utils.enableLayout(HomeActivity.this,animation,layout,progressBar);
                    categoryClasList = response.body();
                    if(categoryClasList.get(0).getSuccess())
                    {
                        categoryAdapter = new CategoryAdapter(HomeActivity.this,categoryClasList);
                        recycler_view_category.setAdapter(categoryAdapter);
                        LinearLayoutManager llm = new LinearLayoutManager(HomeActivity.this);
                        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
                        recycler_view_category.setLayoutManager(llm);
                    }
                    else
                    {
                        Toasty.error(HomeActivity.this,categoryClasList.get(0).getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    Toasty.error(HomeActivity.this,"Some went wrong please try again later.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<CategoryClas>> call, Throwable t) {
                Utils.enableLayout(HomeActivity.this,animation,layout,progressBar);
                Toasty.error(HomeActivity.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void load_featured_Show()
    {
        SOService soService = ApiUtils.getSOService();
        soService.getShowResponse(ApiUtils.BASE_URL,Utils.getSimpleTextBody("load_featured_show_for_user_app")).enqueue(new Callback<List<ShowClass>>() {
            @Override
            public void onResponse(Call<List<ShowClass>> call, Response<List<ShowClass>> response) {
                if(response.isSuccessful())
                {
                    Utils.enableLayout(HomeActivity.this,animation,layout,progressBar);
                    showClassList = response.body();
                    if(showClassList.get(0).getSuccess())
                    {
                        showAdapter = new ShowAdapter(HomeActivity.this,showClassList);
                        recycler_view_fshow.setAdapter(showAdapter);
                        LinearLayoutManager llm = new LinearLayoutManager(HomeActivity.this);
                        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
                        recycler_view_fshow.setLayoutManager(llm);
                    }
                    else
                    {
                        Toasty.error(HomeActivity.this,showClassList.get(0).getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    Toasty.error(HomeActivity.this,"Some went wrong please try again later.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<ShowClass>> call, Throwable t) {
                Utils.enableLayout(HomeActivity.this,animation,layout,progressBar);
                Toasty.error(HomeActivity.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void load_Shows()
    {
        SOService soService = ApiUtils.getSOService();
        soService.getShowResponse(ApiUtils.BASE_URL,Utils.getSimpleTextBody("load_show_for_user_app")).enqueue(new Callback<List<ShowClass>>() {
            @Override
            public void onResponse(Call<List<ShowClass>> call, Response<List<ShowClass>> response) {
                if(response.isSuccessful())
                {

                    Utils.enableLayout(HomeActivity.this,animation,layout,progressBar);
                    showClassList = response.body();
                    if(showClassList.get(0).getSuccess())
                    {
                        
                        showAdapter = new ShowAdapter(HomeActivity.this,showClassList);
                        recycler_view_show.setAdapter(showAdapter);
                        LinearLayoutManager llm = new LinearLayoutManager(HomeActivity.this);
                        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
                        recycler_view_show.setLayoutManager(llm);

                    }
                    else
                    {

                        Toasty.error(HomeActivity.this,showClassList.get(0).getMessage(), Toast.LENGTH_LONG).show();

                    }
                }
                else
                {
                    Toasty.error(HomeActivity.this,"Some went wrong please try again later.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<ShowClass>> call, Throwable t) {
                Utils.enableLayout(HomeActivity.this,animation,layout,progressBar);
                Toasty.error(HomeActivity.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getUserInfo()
    {
        SOService soService = ApiUtils.getSOService();
        soService.getUserDetailResponse(ApiUtils.BASE_URL,Utils.getSimpleTextBody("load_user_detail_by_id"),Utils.getSimpleTextBody(user_id)).enqueue(new Callback<UserDetailClass>() {
            @Override
            public void onResponse(Call<UserDetailClass> call, Response<UserDetailClass> response) {
                if(response.isSuccessful())
                {
                    tv_uname.setText(response.body().getUserName());
                    tv_uEmail.setText(response.body().getUserEmail());
                    Glide.with(HomeActivity.this).load(response.body().getUserImage()).into(imageView);
                }
            }

            @Override
            public void onFailure(Call<UserDetailClass> call, Throwable t) {
                Toasty.error(HomeActivity.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else
        {
            super.onBackPressed();
            Animation.fade(HomeActivity.this);
            moveTaskToBack(true);
            System.exit(1);
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.home, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_category) {

            // Handle the camera action
            startActivity(new Intent(HomeActivity.this,CategoryActivity.class));
            Animation.slideDown(HomeActivity.this);

        } else if (id == R.id.nav_feature_show) {
            // Handle the camera action
            startActivity(new Intent(HomeActivity.this,ShowFeturedActivity.class));
            Animation.slideDown(HomeActivity.this);
        } else if (id == R.id.nav_show) {
            // Handle the camera action
            startActivity(new Intent(HomeActivity.this,ShowActivity.class));
            Animation.slideDown(HomeActivity.this);


        } else if (id == R.id.nav_search) {
            startActivity(new Intent(HomeActivity.this,SearchActivity.class));
            Animation.fade(HomeActivity.this);

        }
        else if (id == R.id.nav_my_booking)
        {
            startActivity(new Intent(HomeActivity.this, MyBookingActivity.class));
            Animation.fade(HomeActivity.this);
        }
        else if (id == R.id.nav_profile) {
            startActivity(new Intent(HomeActivity.this,Profile_Activity.class));
            Animation.fade(HomeActivity.this);

        } else if (id == R.id.nav_about) {

            startActivity(new Intent(HomeActivity.this,AboutActivity.class));
            Animation.fade(HomeActivity.this);

        } else if (id == R.id.nav_how_to) {

            startActivity(new Intent(HomeActivity.this,HowTouseActivity.class));
            Animation.fade(HomeActivity.this);

        } else if (id == R.id.nav_logout) {
            startActivity(new Intent(HomeActivity.this,MainActivity.class));
            Toasty.success(HomeActivity.this,"Successfully Logout",Toast.LENGTH_SHORT).show();
            Prefs.putBoolean("isLogin",false);
            Animation.fade(HomeActivity.this);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
