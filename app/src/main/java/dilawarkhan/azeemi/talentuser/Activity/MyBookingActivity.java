package dilawarkhan.azeemi.talentuser.Activity;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.pixplicity.easyprefs.library.Prefs;

import java.util.List;

import dilawarkhan.azeemi.talentuser.Adapter.MyBookingListAdapter;
import dilawarkhan.azeemi.talentuser.Api.ApiUtils;
import dilawarkhan.azeemi.talentuser.Api.SOService;
import dilawarkhan.azeemi.talentuser.Api.Utils;
import dilawarkhan.azeemi.talentuser.Classes.Animation;
import dilawarkhan.azeemi.talentuser.Classes.MyBookingListClass;
import dilawarkhan.azeemi.talentuser.R;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyBookingActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ProgressBar progressBar;
    private android.view.animation.Animation animation;
    private RecyclerView recyclerView;
    private List<MyBookingListClass> myBookingListClassList;
    private MyBookingListAdapter myBookingListAdapter;
    private String user_id;
    private RelativeLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booking);

        toolbar = findViewById(R.id.toolbar_myShow);
        toolbar.setTitle("My Bookings");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = findViewById(R.id.recycler_view);
        progressBar = findViewById(R.id.progress);
        layout = findViewById(R.id.freez_layout);

        user_id = Prefs.getString("user_id","0");

        if(Utils.isOnline(MyBookingActivity.this))
        {
            Utils.disable_layout(MyBookingActivity.this,animation,layout,progressBar);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run()
                {
                    try
                    {
                        LoadMybooking();
                    }
                    catch (Exception e)
                    {
                        Toasty.error(MyBookingActivity.this,e.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                }
            }, 2000);
        }
        else
        {
            Utils.error_dialog(MyBookingActivity.this,"Internet Error","Internet Not Found");
        }

    }

    private void LoadMybooking()
    {
        SOService soService = ApiUtils.getSOService();
        soService.getMyBookingResponse(ApiUtils.BASE_URL, Utils.getSimpleTextBody("load_user_booked_shows"),Utils.getSimpleTextBody(user_id)).enqueue(new Callback<List<MyBookingListClass>>() {

            @Override
            public void onResponse(Call<List<MyBookingListClass>> call, Response<List<MyBookingListClass>> response)
            {
                if(response.isSuccessful())
                {
                    Utils.enableLayout(MyBookingActivity.this,animation,layout,progressBar);
                    myBookingListClassList = response.body();
                    if(myBookingListClassList.get(0).getSuccess())
                    {
                        myBookingListAdapter = new MyBookingListAdapter(MyBookingActivity.this,myBookingListClassList);
                        recyclerView.setAdapter(myBookingListAdapter);
                        LinearLayoutManager llm = new LinearLayoutManager(MyBookingActivity.this);
                        llm.setOrientation(LinearLayoutManager.VERTICAL);
                        recyclerView.setLayoutManager(llm);
                    }
                    else
                    {
                        Utils.error_dialog(MyBookingActivity.this,"Error", myBookingListClassList.get(0).getMessage());
                    }
                }
                else
                {
                    Utils.error_dialog(MyBookingActivity.this,"Error", response.message());
                }
            }

            @Override
            public void onFailure(Call<List<MyBookingListClass>> call, Throwable t) {
                Utils.enableLayout(MyBookingActivity.this,animation,layout,progressBar);
                Toasty.error(MyBookingActivity.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        Animation.fade(MyBookingActivity.this);
        return true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Animation.fade(MyBookingActivity.this);
    }
}
