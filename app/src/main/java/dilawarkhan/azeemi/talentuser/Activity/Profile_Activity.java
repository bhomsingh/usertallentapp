package dilawarkhan.azeemi.talentuser.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.abdeveloper.library.MultiSelectDialog;
import com.abdeveloper.library.MultiSelectModel;
import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.pixplicity.easyprefs.library.Prefs;

import java.io.File;
import java.util.ArrayList;

import dilawarkhan.azeemi.talentuser.Api.ApiUtils;
import dilawarkhan.azeemi.talentuser.Api.SOService;
import dilawarkhan.azeemi.talentuser.Api.Utils;
import dilawarkhan.azeemi.talentuser.Classes.Animation;
import dilawarkhan.azeemi.talentuser.Classes.ResponseClass;
import dilawarkhan.azeemi.talentuser.Classes.UserDetailClass;
import dilawarkhan.azeemi.talentuser.Classes.UserRegistration;
import dilawarkhan.azeemi.talentuser.R;
import es.dmoral.toasty.Toasty;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Profile_Activity extends AppCompatActivity {

    private Toolbar toolbar;
    private ImageView img_profile;
    private EditText edit_user_Name,edit_user_Email,edit_user_pass,edit_d_of_b,edit_user_gender,edit_user_contant,edit_user_address;
    private Button btn_Update;
    private String photoPath="";
    private String user_id,profileUserid, str_intrest = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_);

        toolbar = findViewById(R.id.toolbar_myShow);
        toolbar.setTitle(" Profile ");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        user_id = Prefs.getString("user_id","0");

        edit_user_Name =  findViewById(R.id.edit_txt_name);
        edit_user_Email = findViewById(R.id.edit_txt_email);
        edit_user_pass = findViewById(R.id.edit_txt_pass);
        edit_d_of_b = findViewById(R.id.edit_txt_db);
        edit_user_gender = findViewById(R.id.edit_txt_gender);
        edit_user_contant = findViewById(R.id.edit_txt_contant);
        edit_user_address = findViewById(R.id.edit_txt_address);

        img_profile = findViewById(R.id.img_profile);
        btn_Update = findViewById(R.id.btn_update);

        if(Utils.isOnline(this)){

            loadProfileActivity();

        }else{
            Toasty.error(getApplicationContext(),"Internet Not Found",Toast.LENGTH_LONG).show();
        }

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePicker.create(Profile_Activity.this) // Activity or Fragment
                        .folderMode(true) // folder mode (false by default)
                        .toolbarFolderTitle("Select Image") // folder selection title
                        .toolbarImageTitle("Tap to select") // image selection title
                        .single() // single mode
                        .start();
            }
        });

        btn_Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Click Button Update
                if(Utils.isOnline(Profile_Activity.this)){

                    updateProfile();

                }else{
                    Toasty.error(getApplicationContext(),"Internet Not Found",Toast.LENGTH_LONG).show();
                }

            }
        });

        //List of Countries with Name and Id
        final ArrayList<MultiSelectModel> listOfCountries= new ArrayList<>();
        listOfCountries.add(new MultiSelectModel(1,"Dance"));
        listOfCountries.add(new MultiSelectModel(2,"Nature"));
        listOfCountries.add(new MultiSelectModel(3,"Business"));
        listOfCountries.add(new MultiSelectModel(4,"Reading"));
        listOfCountries.add(new MultiSelectModel(5,"Technology"));
        listOfCountries.add(new MultiSelectModel(6,"Photography"));
        listOfCountries.add(new MultiSelectModel(7,"Fitness"));
        listOfCountries.add(new MultiSelectModel(8,"Party"));
        listOfCountries.add(new MultiSelectModel(9,"Beauty"));
        listOfCountries.add(new MultiSelectModel(10,"Creative"));

        findViewById(R.id.btn_intrest).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MultiSelectDialog multiSelectDialog = new MultiSelectDialog()
                        .title("Select interest") //setting title for dialog getResources().getString(R.string.multi_select_dialog_title)
                        .titleSize(25)
                        .positiveText("Done")
                        .negativeText("Cancel")
                        .setMinSelectionLimit(1) //you can set minimum checkbox selection limit (Optional)
                        .setMaxSelectionLimit(listOfCountries.size()) //you can set maximum checkbox selection limit (Optional)
                        //.preSelectIDsList(alreadySelectedCountries) //List of ids that you need to be selected
                        .multiSelectList(listOfCountries) // the multi select model list with ids and name
                        .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                            @Override
                            public void onSelected(ArrayList<Integer> selectedIds, ArrayList<String> selectedNames, String dataString) {
                                //will return list of selected IDS
//                                for (int i = 0; i < selectedIds.size(); i++) {
//                                    Toast.makeText(Profile_Activity.this, "Selected Ids : " + selectedIds.get(i) + "\n" +
//                                            "Selected Names : " + selectedNames.get(i) + "\n" +
//                                            "DataString : " + dataString, Toast.LENGTH_SHORT).show();
//                                }
                                str_intrest = dataString;


                            }

                            @Override
                            public void onCancel() {
                                //Log.d(TAG,"Dialog cancelled");
                            }


                        });

                multiSelectDialog.show(getSupportFragmentManager(), "multiSelectDialog");
            }
        });

    }// close the ONCreate

    private void loadProfileActivity(){

        SOService soService = ApiUtils.getSOService();
        soService.getProfileResponse(ApiUtils.BASE_URL,Utils.getSimpleTextBody("load_user_detail_by_id")
        ,Utils.getSimpleTextBody(user_id)).enqueue(new Callback<UserDetailClass>() {

            @Override
            public void onResponse(Call<UserDetailClass> call, Response<UserDetailClass> response) {
                if(response.isSuccessful()) {

                    profileUserid = response.body().getUserId();
                    Glide.with(Profile_Activity.this).load(response.body().getUserImage()).into(img_profile);
                    edit_user_Name.setText(response.body().getUserName());
                    edit_user_Email.setText(response.body().getUserEmail());
                    edit_d_of_b.setText(response.body().getUserDob());
                    edit_user_gender.setText(response.body().getUserGender());
                    edit_user_contant.setText(response.body().getUserPhone());
                    edit_user_address.setText(response.body().getUserAddress());

                } else
                    {
                        Utils.error_dialog(Profile_Activity.this,"Error", response.body().toString());
                    }
            }

            @Override
            public void onFailure(Call<UserDetailClass> call, Throwable t) {

            }
        });
 }// close the loadProfileActvity

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (ImagePicker.shouldHandle(requestCode, resultCode, data))
            {
                Image image = ImagePicker.getFirstImageOrNull(data);
                photoPath = image.getPath();
                Bitmap bmImg = BitmapFactory.decodeFile(photoPath);
                img_profile.setImageBitmap(bmImg);
            }
        }catch (Exception e){
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private void updateProfile(){

        File photofile = null;
        if (!photoPath.isEmpty() && photoPath != null) {
            photofile  = new File(photoPath);
        }

        // Parsing any Media type file
        RequestBody requestBodyImage;
        MultipartBody.Part body_Iamge = null;

        if(photofile != null){
            requestBodyImage = RequestBody.create(MediaType.parse("*/*"), photofile);
            body_Iamge = MultipartBody.Part.createFormData("pro_image", photofile.getName(), requestBodyImage);

        }

        SOService soService = ApiUtils.getSOService();
        soService.getProfileUpdateResponse(ApiUtils.BASE_URL,
                Utils.getSimpleTextBody("update_user_profile"),
                Utils.getSimpleTextBody(edit_user_Name.getText().toString()),
                Utils.getSimpleTextBody(edit_user_Email.getText().toString()),
                Utils.getSimpleTextBody(edit_user_pass.getText().toString()),
                Utils.getSimpleTextBody(edit_d_of_b.getText().toString()),
                Utils.getSimpleTextBody(edit_user_gender.getText().toString()),
                Utils.getSimpleTextBody(edit_user_contant.getText().toString()),
                Utils.getSimpleTextBody(edit_user_address.getText().toString()),
                Utils.getSimpleTextBody(profileUserid),
                Utils.getSimpleTextBody(str_intrest),body_Iamge).enqueue(new Callback<ResponseClass>() {

                      @Override
                      public void onResponse(Call<ResponseClass> call, Response<ResponseClass> response) {
                          if(response.isSuccessful())
                          {
                              if(response.body().getSuccess())
                              {
                                  Utils.success_dialog(Profile_Activity.this,"Success", response.body().getMessage());
                              }
                              else
                              {
                                  Utils.error_dialog(Profile_Activity.this,"Error", response.body().getMessage());
                              }
                          }
                      }

                      @Override
                      public void onFailure(Call<ResponseClass> call, Throwable t) {

                          Utils.error_dialog(Profile_Activity.this,"Error", t.getMessage());

                      }
                });
    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        Animation.slideUp(Profile_Activity.this);
        return true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Animation.slideUp(Profile_Activity.this);
    }
}
