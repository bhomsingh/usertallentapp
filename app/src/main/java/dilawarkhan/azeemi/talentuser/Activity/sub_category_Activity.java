package dilawarkhan.azeemi.talentuser.Activity;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.List;

import dilawarkhan.azeemi.talentuser.Adapter.ShowFeaturedOrignalAdapter;
import dilawarkhan.azeemi.talentuser.Adapter.Sub_categoryOrignalAdapter;
import dilawarkhan.azeemi.talentuser.Api.ApiUtils;
import dilawarkhan.azeemi.talentuser.Api.SOService;
import dilawarkhan.azeemi.talentuser.Api.Utils;
import dilawarkhan.azeemi.talentuser.Classes.Animation;
import dilawarkhan.azeemi.talentuser.Classes.CategoryClas;
import dilawarkhan.azeemi.talentuser.Classes.ShowClass;
import dilawarkhan.azeemi.talentuser.Classes.Sub_cateegoryClass;
import dilawarkhan.azeemi.talentuser.R;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class sub_category_Activity extends AppCompatActivity {

    private Toolbar toolbar;
    private ProgressBar progressBar, progressBottom;
    private android.view.animation.Animation animation;
    private RecyclerView recyclerView;
    private RelativeLayout layout;
    private List<Sub_cateegoryClass> categoryClasList;
    private Sub_categoryOrignalAdapter sub_categoryOrignalAdapter;
    private LinearLayoutManager manager;
    private Boolean isScrolling = false;
    private int currentItems, totalItems = 0, scrollOutItems;

    String cat_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category_);

        toolbar = findViewById(R.id.toolbar_myShow);
        toolbar.setTitle("Sub Category");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cat_id = getIntent().getStringExtra("cat_id");

        recyclerView = findViewById(R.id.recycler_view);
        progressBar = findViewById(R.id.progress);
        progressBottom = findViewById(R.id.progressBottom);
        layout = findViewById(R.id.freez_layout);

        if (Utils.isOnline(sub_category_Activity.this)) {
            Utils.disable_layout(sub_category_Activity.this, animation, layout, progressBar);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        Loadsub_categoryList();
                    } catch (Exception e) {
                        Toasty.error(sub_category_Activity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, 2000);
        } else {
            Utils.error_dialog(sub_category_Activity.this, "Internet Error", "Internet Not Found");
        }

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = manager.getChildCount();
                totalItems = manager.getItemCount();

                scrollOutItems = manager.findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                        //Fetch new Data
                        isScrolling = false;
                        Utils.EnableProgressBar(progressBottom);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    progressBottom.setVisibility(View.GONE);
                                    LoadSub_ListPagination();
                                } catch (Exception e) {
                                    Toasty.error(sub_category_Activity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, 2000);
                    }
                }
            }
        });
    }// close the ONCreate Methord

    private void Loadsub_categoryList(){

        SOService soService = ApiUtils.getSOService();
        soService.getSub_catResponse(ApiUtils.BASE_URL, Utils.getSimpleTextBody("load_sub_category_list")
                ,Utils.getSimpleTextBody(cat_id), Utils.getSimpleTextBody(String.valueOf(totalItems))).enqueue(new Callback<List<Sub_cateegoryClass>>() {

           @Override
            public void onResponse(Call<List<Sub_cateegoryClass>> call, Response<List<Sub_cateegoryClass>> response) {

                if(response.isSuccessful())
                {
                    Utils.enableLayout(sub_category_Activity.this,animation,layout,progressBar);
                    categoryClasList = response.body();
                    if(categoryClasList.get(0).getSuccess())
                    {

                        sub_categoryOrignalAdapter = new Sub_categoryOrignalAdapter(sub_category_Activity.this,categoryClasList);
                        recyclerView.setAdapter(sub_categoryOrignalAdapter);
                        manager = new LinearLayoutManager(sub_category_Activity.this);
                        manager.setOrientation(LinearLayoutManager.VERTICAL);
                        recyclerView.setLayoutManager(manager);
                    }
                    else
                    {
                        Utils.error_dialog(sub_category_Activity.this,"Error", categoryClasList.get(0).getMessage());
                    }
                }
                else
                {
                    Utils.error_dialog(sub_category_Activity.this,"Error", response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Sub_cateegoryClass>> call, Throwable t) {
                Toasty.error(sub_category_Activity.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void  LoadSub_ListPagination(){

        SOService soService = ApiUtils.getSOService();
        soService.getSub_catResponse(ApiUtils.BASE_URL, Utils.getSimpleTextBody("load_sub_category_list"),
                Utils.getSimpleTextBody(cat_id),Utils.getSimpleTextBody(String.valueOf(totalItems))).enqueue(new Callback<List<Sub_cateegoryClass>>() {

            @Override
            public void onResponse(Call<List<Sub_cateegoryClass>> call, Response<List<Sub_cateegoryClass>> response) {
                if(response.isSuccessful())
                {
                    progressBottom.setVisibility(View.GONE);
                    categoryClasList = response.body();
                    if(categoryClasList.get(0).getSuccess())
                    {
                        sub_categoryOrignalAdapter.addNewData(categoryClasList);
                    }
                    else
                    {
                        Utils.error_dialog(sub_category_Activity.this,"Error", categoryClasList.get(0).getMessage());
                    }
                }
                else
                {
                    Utils.error_dialog(sub_category_Activity.this,"Error", response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Sub_cateegoryClass>> call, Throwable t) {
                progressBottom.setVisibility(View.GONE);
                Toasty.error(sub_category_Activity.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        Animation.slideUp(sub_category_Activity.this);
        return true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Animation.slideUp(sub_category_Activity.this);
    }
}