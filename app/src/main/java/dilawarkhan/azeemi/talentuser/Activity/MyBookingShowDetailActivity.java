package dilawarkhan.azeemi.talentuser.Activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import dilawarkhan.azeemi.talentuser.Api.ApiUtils;
import dilawarkhan.azeemi.talentuser.Api.SOService;
import dilawarkhan.azeemi.talentuser.Api.Utils;
import dilawarkhan.azeemi.talentuser.Classes.Animation;
import dilawarkhan.azeemi.talentuser.Classes.MyBookShowDetailClass;
import dilawarkhan.azeemi.talentuser.R;
import es.dmoral.toasty.Toasty;
import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyBookingShowDetailActivity extends AppCompatActivity {

    private ImageView banner_image;
    private TextView TV_status, TV_title, TV_short_description, TV_date, TV_start_time, tv_end_time, TV_long_description, TV_H_user_count, TV_price;
    private Button btn_comment, btn_live;
    private RelativeLayout layout;
    private ProgressBar progressBar;
    private android.view.animation.Animation animation;
    private String show_id, user_id;

    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booking_show_detail);

        banner_image = findViewById(R.id.banner_image);
        TV_status = findViewById(R.id.TV_status);
        TV_title = findViewById(R.id.TV_title);
        TV_short_description = findViewById(R.id.TV_short_description);
        TV_date = findViewById(R.id.TV_date);
        TV_start_time = findViewById(R.id.TV_start_time);
        tv_end_time = findViewById(R.id.tv_end_time);
        TV_long_description = findViewById(R.id.TV_long_description);
        TV_H_user_count = findViewById(R.id.TV_H_user_count);
        TV_price = findViewById(R.id.TV_price);
        btn_live = findViewById(R.id.btn_live);
        btn_comment = findViewById(R.id.btn_comment);
        layout = findViewById(R.id.freez_layout);
        progressBar = findViewById(R.id.progress);

        show_id = getIntent().getStringExtra("show_id");

        btn_live.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MyBookingShowDetailActivity.this, PlayerActivity.class);
                i.putExtra("show_id",show_id);
                startActivity(i);
                Animation.fade(MyBookingShowDetailActivity.this);
            }
        });

        btn_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MyBookingShowDetailActivity.this, CommentActivity.class);
                i.putExtra("show_id",show_id);
                startActivity(i);
                Animation.fade(MyBookingShowDetailActivity.this);
            }
        });

        if(Utils.isOnline(MyBookingShowDetailActivity.this))
        {
            Utils.disable_layout(MyBookingShowDetailActivity.this,animation,layout,progressBar);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run()
                {
                    try
                    {
                        LoadMyShowBookingDetail();
                    }
                    catch (Exception e)
                    {
                        Toasty.error(MyBookingShowDetailActivity.this,e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, 2000);
        }
        else
        {
            Utils.error_dialog(MyBookingShowDetailActivity.this,"Internet Error","Internet Not Found");
        }
    }

    private void LoadMyShowBookingDetail()
    {
        SOService soService = ApiUtils.getSOService();
        soService.getMyBookShowDetailResponse(ApiUtils.BASE_URL, Utils.getSimpleTextBody("load_user_booked_shows_detail"),Utils.getSimpleTextBody(show_id)).enqueue(new Callback<MyBookShowDetailClass>() {
            @Override
            public void onResponse(Call<MyBookShowDetailClass> call, Response<MyBookShowDetailClass> response)
            {
                if(response.isSuccessful())
                {
                    layout.setVisibility(View.VISIBLE);
                    Utils.enableLayout(MyBookingShowDetailActivity.this,animation,layout,progressBar);
                    if(response.body().getSuccess())
                    {
                        Glide.with(MyBookingShowDetailActivity.this).load(response.body().getShowBannerImage()).into(banner_image);
                        if(response.body().getLiveBtn())
                        {
                            btn_live.setVisibility(View.VISIBLE);
                        }
                        TV_title.setText(response.body().getShowTitle());
                        TV_short_description.setText(response.body().getShowShortDescription());
                        TV_date.setText(response.body().getShowDate());
                        TV_start_time.setText(response.body().getShowStartTime());
                        tv_end_time.setText(response.body().getShowEndTime());
                        TV_long_description.setText(response.body().getShowDescription());
                        TV_H_user_count.setText(response.body().getUserCount().toString());
                        TV_price.setText(response.body().getShowFee());
                        TV_status.setText(response.body().getShowStatus());
                    }
                    else
                    {
                        Utils.error_dialog(MyBookingShowDetailActivity.this,"Error", response.body().getMessage());
                    }
                }
                else
                {
                    Utils.error_dialog(MyBookingShowDetailActivity.this,"Error", response.message());
                }
            }

            @Override
            public void onFailure(Call<MyBookShowDetailClass> call, Throwable t) {
                layout.setVisibility(View.VISIBLE);
                Utils.enableLayout(MyBookingShowDetailActivity.this,animation,layout,progressBar);
                Utils.error_dialog(MyBookingShowDetailActivity.this,"Error", t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Animation.fade(MyBookingShowDetailActivity.this);
    }
}
