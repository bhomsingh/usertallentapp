package dilawarkhan.azeemi.talentuser.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.shashank.sony.fancywalkthroughlib.FancyWalkthroughActivity;
import com.shashank.sony.fancywalkthroughlib.FancyWalkthroughCard;

import java.util.ArrayList;
import java.util.List;

import dilawarkhan.azeemi.talentuser.Classes.Animation;
import dilawarkhan.azeemi.talentuser.R;

public class HowTouseActivity extends FancyWalkthroughActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_how_touse);

        FancyWalkthroughCard fancywalkthroughCard1 = new FancyWalkthroughCard("Shows", " Find the show which is Best",R.drawable.ic_home_black_24dp);
        FancyWalkthroughCard fancywalkthroughCard2 = new FancyWalkthroughCard("Sub Category Shows ", " find the Sub Category chouse which is Best Category",R.drawable.ic_home_black_24dp);
        FancyWalkthroughCard fancywalkthroughCard3 = new FancyWalkthroughCard("Detial Shows ", "Show the  Detial over All categorys ",R.drawable.ic_details_black_24dp);
        FancyWalkthroughCard fancywalkthroughCard4 = new FancyWalkthroughCard("Booking Shows ", " Show Booking Which is Best Shows  ",R.drawable.ic_book_black_24dp);

        fancywalkthroughCard1.setBackgroundColor(R.color.white);
        fancywalkthroughCard1.setIconLayoutParams(300,300,0,0,0,0);
        fancywalkthroughCard2.setBackgroundColor(R.color.white);
        fancywalkthroughCard2.setIconLayoutParams(300,300,0,0,0,0);
        fancywalkthroughCard3.setBackgroundColor(R.color.white);
        fancywalkthroughCard3.setIconLayoutParams(300,300,0,0,0,0);
        fancywalkthroughCard4.setBackgroundColor(R.color.white);
        fancywalkthroughCard4.setIconLayoutParams(300,300,0,0,0,0);
        List<FancyWalkthroughCard> pages = new ArrayList<>();

        pages.add(fancywalkthroughCard1);
        pages.add(fancywalkthroughCard2);
        pages.add(fancywalkthroughCard3);
        pages.add(fancywalkthroughCard4);

        for (FancyWalkthroughCard page : pages) {
            page.setTitleColor(R.color.black);
            page.setDescriptionColor(R.color.black);
        }

        setFinishButtonTitle("Get Started");
        showNavigationControls(true);
        setColorBackground(R.color.colorAccent);
        //setImageBackground(R.drawable.restaurant);
        setInactiveIndicatorColor(R.color.grey_600);
        setActiveIndicatorColor(R.color.colorAccent);
        setOnboardPages(pages);

    }

    @Override
    public void onFinishButtonPressed() {
        startActivity(new Intent(HowTouseActivity.this,HomeActivity.class));
        Animation.fade(HowTouseActivity.this);
    }
}
