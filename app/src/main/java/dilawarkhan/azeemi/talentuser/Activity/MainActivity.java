package dilawarkhan.azeemi.talentuser.Activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.pixplicity.easyprefs.library.Prefs;

import dilawarkhan.azeemi.talentuser.Api.ApiUtils;
import dilawarkhan.azeemi.talentuser.Api.SOService;
import dilawarkhan.azeemi.talentuser.Api.Utils;
import dilawarkhan.azeemi.talentuser.Classes.Animation;
import dilawarkhan.azeemi.talentuser.Classes.LoginClass;
import dilawarkhan.azeemi.talentuser.R;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private EditText et_email, et_pass;
    private Button btn_sign_in;
    private RelativeLayout layout;
    private ProgressBar progressBar;
    private Spinner gender;
    private android.view.animation.Animation animation;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.text_view_sign_up).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Sign_Up.class));
                Animation.fade(MainActivity.this);
            }
        });
        Boolean isLogin = Prefs.getBoolean("isLogin",false);
        if(isLogin)
        {
            startActivity(new Intent(MainActivity.this, HomeActivity.class));
            Animation.fade(MainActivity.this);
        }

        et_email     = findViewById(R.id.edit_text_name);
        et_pass      = findViewById(R.id.edit_text_pass);
        btn_sign_in  = findViewById(R.id.signin_button);
        layout       = findViewById(R.id.freez_layout);
        progressBar  = findViewById(R.id.progress);

        btn_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Utils.isOnline(MainActivity.this))
                {
                    if(TextUtils.isEmpty(et_email.getText().toString()))
                    {
                        et_email.setError("Required");
                        et_email.setFocusable(true);
                    }
                    else if(TextUtils.isEmpty(et_pass.getText().toString()))
                    {
                        et_pass.setError("Required");
                        et_pass.setFocusable(true);
                    }
                    else
                    {
                        Utils.disable_layout(MainActivity.this,animation,layout,progressBar);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run()
                            {
                                login();
                            }
                        }, 2000);
                    }
                }
                else
                {
                    Utils.error_dialog(MainActivity.this,"Internet Error","Internet Not Found");
                }
            }
        });
    }

    private void login()
    {
        SOService soService = ApiUtils.getSOService();
        soService.getLoginResponse(ApiUtils.BASE_URL,
                Utils.getSimpleTextBody("user_login"),
                Utils.getSimpleTextBody(et_email.getText().toString()),
                Utils.getSimpleTextBody(et_pass.getText().toString())).enqueue(new Callback<LoginClass>() {
            @Override
            public void onResponse(Call<LoginClass> call, Response<LoginClass> response) {
                if(response.isSuccessful())
                {
                    Utils.enableLayout(MainActivity.this,animation,layout,progressBar);
                    if(response.body().getSuccess())
                    {
                        Toasty.success(MainActivity.this,response.body().getMessage(),Toast.LENGTH_SHORT).show();
                        Prefs.putBoolean("isLogin",true);
                        Prefs.putString("user_id",response.body().getUserId());
                        startActivity(new Intent(MainActivity.this, HomeActivity.class));
                        Animation.fade(MainActivity.this);
                    }
                    else
                    {
                        Utils.error_dialog(MainActivity.this,"Error", response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginClass> call, Throwable t) {
                Utils.enableLayout(MainActivity.this,animation,layout,progressBar);
                Toasty.error(MainActivity.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

}
