package dilawarkhan.azeemi.talentuser.Activity;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;


import java.util.List;

import dilawarkhan.azeemi.talentuser.Adapter.SearchAdapter;
import dilawarkhan.azeemi.talentuser.Adapter.ShowOrignalAdapter;
import dilawarkhan.azeemi.talentuser.Api.ApiUtils;
import dilawarkhan.azeemi.talentuser.Api.SOService;
import dilawarkhan.azeemi.talentuser.Api.Utils;
import dilawarkhan.azeemi.talentuser.Classes.Animation;
import dilawarkhan.azeemi.talentuser.Classes.ShowClass;
import dilawarkhan.azeemi.talentuser.R;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFilterActivity extends AppCompatActivity {

    private String cat_id,subCat_id,et_keyword,fect_id;
    private Toolbar toolbar;
    private ProgressBar progressBar, progressBottom;
    private android.view.animation.Animation animation;
    private RecyclerView recyclerView;
    private RelativeLayout layout;
    private List<ShowClass> showClassList;
    private SearchAdapter showSearchAdapter;
    private LinearLayoutManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_filter);

        toolbar = findViewById(R.id.toolbar_myShow);
        toolbar.setTitle("Search Filter Activity");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

         cat_id = getIntent().getExtras().getString("cat_id");
         subCat_id = getIntent().getExtras().getString("subCat_id");
         et_keyword = getIntent().getExtras().getString("et_keyword");
         fect_id = getIntent().getExtras().getString("fect_id");

        recyclerView = findViewById(R.id.recycler_view);
        progressBar = findViewById(R.id.progress);
        progressBottom = findViewById(R.id.progressBottom);
        layout = findViewById(R.id.freez_layout);

        if (Utils.isOnline(SearchFilterActivity.this)) {
            Utils.disable_layout(SearchFilterActivity.this, animation, layout, progressBar);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        LoadShowList();
                    } catch (Exception e) {
                        Toasty.error(SearchFilterActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, 2000);
        } else {
            Utils.error_dialog(SearchFilterActivity.this, "Internet Error", "Internet Not Found");
        }
    }

    private void LoadShowList(){

        SOService soService = ApiUtils.getSOService();
        soService.getSearchFilterListForSpinnerResponse(ApiUtils.BASE_URL, Utils.getSimpleTextBody("filter_search_data")
                , Utils.getSimpleTextBody(cat_id)
                ,Utils.getSimpleTextBody(subCat_id)
                ,Utils.getSimpleTextBody(et_keyword)
                ,Utils.getSimpleTextBody(fect_id)).enqueue(new Callback<List<ShowClass>>() {
            @Override
            public void onResponse(Call<List<ShowClass>> call, Response<List<ShowClass>> response) {

                if(response.isSuccessful())
                {
                    Utils.enableLayout(SearchFilterActivity.this,animation,layout,progressBar);
                    showClassList = response.body();
                    if(showClassList.get(0).getSuccess())
                    {
                        showSearchAdapter = new SearchAdapter(SearchFilterActivity.this,showClassList);
                        recyclerView.setAdapter(showSearchAdapter);
                        manager = new LinearLayoutManager(SearchFilterActivity.this);
                        manager.setOrientation(LinearLayoutManager.VERTICAL);
                        recyclerView.setLayoutManager(manager);
                    }
                    else
                    {
                        Utils.error_dialog(SearchFilterActivity.this,"Error", showClassList.get(0).getMessage());
                    }
                }
                else
                {
                    Utils.error_dialog(SearchFilterActivity.this,"Error", response.message());
                }
            }

            @Override
            public void onFailure(Call<List<ShowClass>> call, Throwable t) {
                Toasty.error(SearchFilterActivity.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Animation.slideUp(SearchFilterActivity.this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        Animation.slideUp(SearchFilterActivity.this);
        return true;
    }

}
