package dilawarkhan.azeemi.talentuser.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.MultiplePulse;

import java.io.File;

import dilawarkhan.azeemi.talentuser.Api.ApiUtils;
import dilawarkhan.azeemi.talentuser.Api.SOService;
import dilawarkhan.azeemi.talentuser.Api.Utils;
import dilawarkhan.azeemi.talentuser.Classes.Animation;
import dilawarkhan.azeemi.talentuser.Classes.UserRegistration;
import dilawarkhan.azeemi.talentuser.R;
import es.dmoral.toasty.Toasty;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Sign_Up extends AppCompatActivity {

    private EditText et_name, et_email, et_pass, et_contact;
    private Button sing_up;
    private RelativeLayout layout;
    private ProgressBar progressBar;
    private Spinner gender;
    private android.view.animation.Animation animation;
    private ImageView img_profile;
    private String photoPath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign__up);

        et_name = findViewById(R.id.edit_text_name);
        et_email = findViewById(R.id.edit_text_email);
        et_pass = findViewById(R.id.edit_text_password);
        et_contact = findViewById(R.id.edit_text_phone);
        gender = findViewById(R.id.gender);
        sing_up = findViewById(R.id.signup_button);
        img_profile = findViewById(R.id.img_profile);

        layout       = findViewById(R.id.freez_layout);
        progressBar = findViewById(R.id.progress);

        findViewById(R.id.text_view_sign_up).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Sign_Up.this, MainActivity.class));
                Animation.fade(Sign_Up.this);
            }
        });

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagePicker.create(Sign_Up.this) // Activity or Fragment
                        .folderMode(true) // folder mode (false by default)
                        .toolbarFolderTitle("Select Image") // folder selection title
                        .toolbarImageTitle("Tap to select") // image selection title
                        .single() // single mode
                        .start();
            }
        });

        sing_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Utils.isOnline(Sign_Up.this))
                {
                    if(TextUtils.isEmpty(et_name.getText().toString()))
                    {
                        et_name.setError("Required");
                        et_name.setFocusable(true);
                    }
                    else if(TextUtils.isEmpty(et_email.getText().toString()))
                    {
                        et_email.setError("Required");
                        et_email.setFocusable(true);
                    }
                    else if(TextUtils.isEmpty(et_pass.getText().toString()))
                    {
                        et_pass.setError("Required");
                        et_pass.setFocusable(true);
                    }
                    else if(TextUtils.isEmpty(et_contact.getText().toString()))
                    {
                        et_contact.setError("Required");
                        et_contact.setFocusable(true);
                    }
                    else if(photoPath.isEmpty())
                    {
                        Toasty.error(Sign_Up.this,"Please Select an image.",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Utils.disable_layout(Sign_Up.this,animation,layout,progressBar);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run()
                            {
                                registration();
                            }
                        }, 2000);
                    }
                }
                else
                {
                    Utils.error_dialog(Sign_Up.this,"Internet Error","Internet Not Found");
                }
            }
        });

        final String[] items = new String[]{"Male", "Female"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        gender.setAdapter(adapter);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try
        {
            if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
                Image image = ImagePicker.getFirstImageOrNull(data);
                photoPath = image.getPath();
                Bitmap bmImg = BitmapFactory.decodeFile(photoPath);
                img_profile.setImageBitmap(bmImg);
            }

        } catch (Exception e)
        {
            Toasty.error(Sign_Up.this,e.getMessage(),Toast.LENGTH_SHORT).show();
        }
    }

    private void registration()
    {
        //pass it like this
        File file = new File(photoPath);
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file_name", file.getName(), requestFile);

        SOService soService = ApiUtils.getSOService();
        soService.getUserRegistrationResponse(ApiUtils.BASE_URL,

                Utils.getSimpleTextBody("user_registration"),
                Utils.getSimpleTextBody(et_name.getText().toString()),
                Utils.getSimpleTextBody(et_email.getText().toString()),
                Utils.getSimpleTextBody(et_pass.getText().toString()),
                Utils.getSimpleTextBody(et_contact.getText().toString()),
                Utils.getSimpleTextBody(gender.getSelectedItem().toString()),body).enqueue(new Callback<UserRegistration>() {

            @Override
            public void onResponse(Call<UserRegistration> call, Response<UserRegistration> response) {
                Utils.enableLayout(Sign_Up.this,animation,layout,progressBar);
                if(response.isSuccessful())
                {
                    if(response.body().getSuccess())
                    {
                        Toasty.success(Sign_Up.this,response.body().getMessage(),Toast.LENGTH_LONG).show();
                        finish();
                    }
                    else
                    {
                        Utils.error_dialog(Sign_Up.this,"Error", response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserRegistration> call, Throwable t) {
                Utils.enableLayout(Sign_Up.this,animation,layout,progressBar);
                Toasty.error(Sign_Up.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
