package dilawarkhan.azeemi.talentuser.Activity;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;

import java.util.ArrayList;
import java.util.List;

import dilawarkhan.azeemi.talentuser.Adapter.CommentAdapter;
import dilawarkhan.azeemi.talentuser.Api.ApiUtils;
import dilawarkhan.azeemi.talentuser.Api.SOService;
import dilawarkhan.azeemi.talentuser.Api.Utils;
import dilawarkhan.azeemi.talentuser.Classes.Animation;
import dilawarkhan.azeemi.talentuser.Classes.ShowCommentClass;
import dilawarkhan.azeemi.talentuser.R;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ProgressBar progressBar;
    private android.view.animation.Animation animation;
    private RecyclerView recyclerView;
    private RelativeLayout layout;
    private List<ShowCommentClass> showCommentClassList = new ArrayList<>();
    private CommentAdapter commentAdapter;
    private String show_id, user_id;
    private EditText et_comment;
    private Button comment_Btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        toolbar = findViewById(R.id.toolbar_myShow);
        toolbar.setTitle("Comments");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        show_id = getIntent().getStringExtra("show_id");
        user_id = Prefs.getString("user_id","0");

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        progressBar = findViewById(R.id.progress);
        layout = findViewById(R.id.freez_layout);

        et_comment = findViewById(R.id.et_comment);
        comment_Btn = findViewById(R.id.comment_Btn);


        initializeAdapter();

        //Load Shows Comment
        if(Utils.isOnline(CommentActivity.this))
        {
            //Pusher Real time perfomance
            RealTimeComment();

            Utils.disable_layout(CommentActivity.this,animation,layout,progressBar);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run()
                {
                    LoadShowComment();
                }
            }, 2000);

        }
        else
        {
            Utils.error_dialog(CommentActivity.this,"Internet Error","Internet Not Found");
        }

        comment_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Utils.isOnline(CommentActivity.this))
                {
                    if(!et_comment.getText().toString().isEmpty())
                    {
                        addNewComment();
                        et_comment.setText("");
                    }
                    else
                    {
                        Toasty.error(CommentActivity.this,"Enter Comment", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Utils.error_dialog(CommentActivity.this,"Internet Error","Internet Not Found");
                }
            }
        });
    }

    //Pusher
    private void RealTimeComment()
    {
        PusherOptions options = new PusherOptions();
        options.setCluster("us2");
        Pusher pusher = new Pusher("9041e4401ef01f080e6c", options);

        Channel channel = pusher.subscribe("comment");

        channel.bind("new-comment-add", new SubscriptionEventListener() {
            @Override
            public void onEvent(String channelName, String eventName, final String data)
            {
                CommentActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run()
                    {
                        if(data != null)
                        {
                            //Toast.makeText(ShowCommentActivity.this, data, Toast.LENGTH_SHORT).show();
                            Gson gson = new Gson();
                            ShowCommentClass showCommentClass = gson.fromJson(data,ShowCommentClass.class);
                            if(showCommentClass.getShowId().equals(show_id))
                            {
                                commentAdapter.addSingleComment(showCommentClass);
                                recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView,null,commentAdapter.getItemCount()-1);
                            }
                        }
                    }
                });
            }
        });

        pusher.connect();
    }

    private void LoadShowComment()
    {
        SOService soService = ApiUtils.getSOService();
        soService.getShowCommentResponse(ApiUtils.BASE_URL, Utils.getSimpleTextBody("load_comment_of_show"),Utils.getSimpleTextBody(show_id)).enqueue(new Callback<List<ShowCommentClass>>() {
            @Override
            public void onResponse(Call<List<ShowCommentClass>> call, Response<List<ShowCommentClass>> response) {
                if(response.isSuccessful())
                {
                    layout.setVisibility(View.VISIBLE);
                    Utils.enableLayout(CommentActivity.this,animation,layout,progressBar);
                    showCommentClassList = response.body();
                    if(showCommentClassList.get(0).getSuccess())
                    {
                        commentAdapter.updateComments(showCommentClassList);
                    }
                    else
                    {
                        Utils.error_dialog(CommentActivity.this,"Error", showCommentClassList.get(0).getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ShowCommentClass>> call, Throwable t) {
                layout.setVisibility(View.VISIBLE);
                Utils.enableLayout(CommentActivity.this,animation,layout,progressBar);
                Utils.error_dialog(CommentActivity.this,"Error", t.getMessage());
            }
        });
    }

    private void addNewComment()
    {
        SOService soService = ApiUtils.getSOService();
        soService.getRealTimeCommentResponse(ApiUtils.BASE_URL,
                Utils.getSimpleTextBody("add_new_comment_of_user"),
                Utils.getSimpleTextBody(show_id),
                Utils.getSimpleTextBody(user_id),
                Utils.getSimpleTextBody(et_comment.getText().toString())).enqueue(new Callback<ShowCommentClass>() {
            @Override
            public void onResponse(Call<ShowCommentClass> call, Response<ShowCommentClass> response) {

            }

            @Override
            public void onFailure(Call<ShowCommentClass> call, Throwable t) {
                Toasty.error(CommentActivity.this,t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        Animation.fade(CommentActivity.this);
        return true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Animation.fade(CommentActivity.this);
    }

    private void initializeAdapter() {

        commentAdapter = new CommentAdapter(CommentActivity.this,showCommentClassList);
        recyclerView.setAdapter(commentAdapter);
        LinearLayoutManager llm = new LinearLayoutManager(CommentActivity.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
    }
}
