package dilawarkhan.azeemi.talentuser.Activity;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.List;
import dilawarkhan.azeemi.talentuser.Adapter.showSubCategoryAdapter;
import dilawarkhan.azeemi.talentuser.Api.ApiUtils;
import dilawarkhan.azeemi.talentuser.Api.SOService;
import dilawarkhan.azeemi.talentuser.Api.Utils;
import dilawarkhan.azeemi.talentuser.Classes.Animation;
import dilawarkhan.azeemi.talentuser.Classes.ShowClass;
import dilawarkhan.azeemi.talentuser.Classes.Sub_cateegoryClass;
import dilawarkhan.azeemi.talentuser.R;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class showSubCategoryActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ProgressBar progressBar, progressBottom;
    private android.view.animation.Animation animation;
    private RecyclerView recyclerView;
    private RelativeLayout layout;
    private List<ShowClass> showSub_catClasses;
    private showSubCategoryAdapter showSubCategoryAdapter;
    private LinearLayoutManager manager;
    private Boolean isScrolling = false;
    private int currentItems, totalItems = 0, scrollOutItems;

    String cat_id, sub_cat_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_sub_category_);

        //Testing
        toolbar = findViewById(R.id.toolbar_myShow);
        toolbar.setTitle("Shows");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cat_id = getIntent().getStringExtra("cat_id");
        sub_cat_id = getIntent().getStringExtra("sub_cat_id");

        recyclerView = findViewById(R.id.recycler_view);
        progressBar = findViewById(R.id.progress);
        progressBottom = findViewById(R.id.progressBottom);
        layout = findViewById(R.id.freez_layout);

        if (Utils.isOnline(showSubCategoryActivity.this)) {
            Utils.disable_layout(showSubCategoryActivity.this, animation, layout, progressBar);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        LoadCategory_sub_categoryList();
                    } catch (Exception e) {
                        Toasty.error(showSubCategoryActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, 2000);
        } else {
            Utils.error_dialog(showSubCategoryActivity.this, "Internet Error", "Internet Not Found");
        }

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = manager.getChildCount();
                totalItems = manager.getItemCount();

                scrollOutItems = manager.findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                        //Fetch new Data
                        isScrolling = false;
                        Utils.EnableProgressBar(progressBottom);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    progressBottom.setVisibility(View.GONE);
                                    showSubCatLoadListPagination();
                                } catch (Exception e) {
                                    Toasty.error(showSubCategoryActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, 2000);
                    }
                }
            }
        });

    }// close the ONCreate Methord

    private void LoadCategory_sub_categoryList(){

        SOService soService = ApiUtils.getSOService();
        soService.getshowsubCategoryResponse(ApiUtils.BASE_URL, Utils.getSimpleTextBody("load_shows_list_ac_category_subCategory")
                ,Utils.getSimpleTextBody(cat_id),Utils.getSimpleTextBody(sub_cat_id), Utils.getSimpleTextBody(String.valueOf(totalItems))).enqueue(new Callback<List<ShowClass>>() {

            @Override
            public void onResponse(Call<List<ShowClass>> call, Response<List<ShowClass>> response) {

                if(response.isSuccessful())
                {

                    Utils.enableLayout(showSubCategoryActivity.this,animation,layout,progressBar);
                    showSub_catClasses = response.body();
                    if(showSub_catClasses.get(0).getSuccess())
                    {
                        showSubCategoryAdapter = new showSubCategoryAdapter(showSubCategoryActivity.this,showSub_catClasses);
                        recyclerView.setAdapter(showSubCategoryAdapter);
                        manager = new LinearLayoutManager(showSubCategoryActivity.this);
                        manager.setOrientation(LinearLayoutManager.VERTICAL);
                        recyclerView.setLayoutManager(manager);
                    }
                    else
                    {
                        Utils.error_dialog(showSubCategoryActivity.this,"Error", showSub_catClasses.get(0).getMessage());
                    }
                }
                else
                {
                    Utils.error_dialog(showSubCategoryActivity.this,"Error", response.message());
                }
            }

            @Override
            public void onFailure(Call<List<ShowClass>> call, Throwable t) {
                Toasty.error(showSubCategoryActivity.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void  showSubCatLoadListPagination(){

        SOService soService = ApiUtils.getSOService();
        soService.getshowsubCategoryResponse(ApiUtils.BASE_URL, Utils.getSimpleTextBody("load_sub_category_list")
                ,Utils.getSimpleTextBody(cat_id),Utils.getSimpleTextBody(sub_cat_id), Utils.getSimpleTextBody(String.valueOf(totalItems))).enqueue(new Callback<List<ShowClass>>() {

            @Override
            public void onResponse(Call<List<ShowClass>> call, Response<List<ShowClass>> response) {
                if(response.isSuccessful())
                {
                    progressBottom.setVisibility(View.GONE);
                    showSub_catClasses = response.body();
                    if(showSub_catClasses.get(0).getSuccess())
                    {
                        showSubCategoryAdapter.addNewData(showSub_catClasses);
                    }
                    else
                    {
                        Utils.error_dialog(showSubCategoryActivity.this,"Error", showSub_catClasses.get(0).getMessage());
                    }
                }
                else
                {
                    Utils.error_dialog(showSubCategoryActivity.this,"Error", response.message());
                }
            }

            @Override
            public void onFailure(Call<List<ShowClass>> call, Throwable t) {
                progressBottom.setVisibility(View.GONE);
                Toasty.error(showSubCategoryActivity.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        Animation.slideUp(showSubCategoryActivity.this);
        return true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Animation.slideUp(showSubCategoryActivity.this);
    }





}