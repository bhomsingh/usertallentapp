package dilawarkhan.azeemi.talentuser.Activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.MultiplePulse;
import com.pixplicity.easyprefs.library.Prefs;

import dilawarkhan.azeemi.talentuser.Api.ApiUtils;
import dilawarkhan.azeemi.talentuser.Api.SOService;
import dilawarkhan.azeemi.talentuser.Api.Utils;
import dilawarkhan.azeemi.talentuser.Classes.Animation;
import dilawarkhan.azeemi.talentuser.Classes.ResponseClass;
import dilawarkhan.azeemi.talentuser.Classes.ShowDetailClass;
import dilawarkhan.azeemi.talentuser.R;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowDetailActivity extends AppCompatActivity {

    private ImageView banner_image;
    private TextView TV_status, TV_title, TV_short_description, TV_date, TV_start_time, tv_end_time, TV_long_description, TV_H_user_count, TV_price;
    private Button btn_comment, btn_book;
    private RelativeLayout layout;
    private ProgressBar progressBar;
    private android.view.animation.Animation animation;
    private String show_id, user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_detail);

        banner_image = findViewById(R.id.banner_image);
        TV_status = findViewById(R.id.TV_status);
        TV_title = findViewById(R.id.TV_title);
        TV_short_description = findViewById(R.id.TV_short_description);
        TV_date = findViewById(R.id.TV_date);
        TV_start_time = findViewById(R.id.TV_start_time);
        tv_end_time = findViewById(R.id.tv_end_time);
        TV_long_description = findViewById(R.id.TV_long_description);
        TV_H_user_count = findViewById(R.id.TV_H_user_count);
        TV_price = findViewById(R.id.TV_price);
        btn_book = findViewById(R.id.btn_book);
        btn_comment = findViewById(R.id.btn_comment);
        layout = findViewById(R.id.freez_layout);
        progressBar = findViewById(R.id.progress);

        show_id = getIntent().getStringExtra("show_id");
        user_id = Prefs.getString("user_id","0");

        if(Utils.isOnline(ShowDetailActivity.this))
        {
            loading();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run()
                {
                    try
                    {
                        LoadShowDetailByID();
                    }
                    catch (Exception e)
                    {
                        Toasty.error(ShowDetailActivity.this,e.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                }
            }, 2000);
        }
        else
        {
            Utils.error_dialog(ShowDetailActivity.this,"Internet Error","Internet Not Found");
        }

        btn_book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Utils.isOnline(ShowDetailActivity.this))
                {
                    Utils.disable_layout(ShowDetailActivity.this,animation,layout,progressBar);
                    layout.setVisibility(View.GONE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run()
                        {
                            try
                            {
                                Booking();
                            }
                            catch (Exception e)
                            {
                                Toasty.error(ShowDetailActivity.this,e.getMessage(),Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, 2000);
                }
                else
                {
                    Utils.error_dialog(ShowDetailActivity.this,"Internet Error","Internet Not Found");
                }
            }
        });

        btn_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ShowDetailActivity.this, CommentActivity.class);
                i.putExtra("show_id",show_id);
                startActivity(i);
                Animation.fade(ShowDetailActivity.this);
            }
        });
    }

    private void loading()
    {
        progressBar.setVisibility(View.VISIBLE);
        Sprite doubleBounce = new MultiplePulse();
        progressBar.setIndeterminateDrawable(doubleBounce);
    }

    private void Booking()
    {
        SOService soService = ApiUtils.getSOService();
        soService.getBookingResponse(ApiUtils.BASE_URL,
                Utils.getSimpleTextBody("show_booking"),
                Utils.getSimpleTextBody(show_id),
                Utils.getSimpleTextBody(user_id),
                Utils.getSimpleTextBody(TV_price.getText().toString())).enqueue(new Callback<ResponseClass>() {
            @Override
            public void onResponse(Call<ResponseClass> call, Response<ResponseClass> response) {
                if(response.isSuccessful())
                {
                    Utils.enableLayout(ShowDetailActivity.this,animation,layout,progressBar);
                    layout.setVisibility(View.VISIBLE);
                    if(response.body().getSuccess())
                    {
                        Utils.success_dialog(ShowDetailActivity.this,"Success", response.body().getMessage());
                    }
                    else
                    {
                        Utils.error_dialog(ShowDetailActivity.this,"Error", response.body().getMessage());
                    }
                }
                else
                {
                    Utils.error_dialog(ShowDetailActivity.this,"Error", response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseClass> call, Throwable t) {
                Utils.enableLayout(ShowDetailActivity.this,animation,layout,progressBar);
                layout.setVisibility(View.VISIBLE);
                Utils.error_dialog(ShowDetailActivity.this,"Error", t.getMessage());
            }
        });
    }

    private void LoadShowDetailByID()
    {
        SOService soService = ApiUtils.getSOService();
        soService.getShowDetailResponse(ApiUtils.BASE_URL, Utils.getSimpleTextBody("load_show_detail_by_id_for_user")
                ,Utils.getSimpleTextBody(show_id)).enqueue(new Callback<ShowDetailClass>() {
            @Override
            public void onResponse(Call<ShowDetailClass> call, Response<ShowDetailClass> response) {
                if(response.isSuccessful())
                {
                    layout.setVisibility(View.VISIBLE);
                    Utils.enableLayout(ShowDetailActivity.this,animation,layout,progressBar);
                    if(response.body().getSuccess())
                    {
                        Glide.with(ShowDetailActivity.this).load(response.body().getShowBannerImage()).into(banner_image);
                        if (response.body().getShowFeatured().equals("1"))
                        {
                            TV_status.setVisibility(View.VISIBLE);
                            TV_status.setText("Featured");
                        }
                        TV_title.setText(response.body().getShowTitle());
                        TV_short_description.setText(response.body().getShowShortDescription());
                        TV_date.setText(response.body().getShowDate());
                        TV_start_time.setText(response.body().getShowStartTime());
                        tv_end_time.setText(response.body().getShowEndTime());
                        TV_long_description.setText(response.body().getShowDescription());
                        TV_H_user_count.setText(response.body().getUserCount().toString());
                        TV_price.setText(response.body().getShowFee());
                    }
                    else
                    {
                        Utils.error_dialog(ShowDetailActivity.this,"Error", response.body().getMessage());
                    }
                }
                else
                {
                    Utils.error_dialog(ShowDetailActivity.this,"Error", response.message());
                }
            }

            @Override
            public void onFailure(Call<ShowDetailClass> call, Throwable t) {
                Utils.enableLayout(ShowDetailActivity.this,animation,layout,progressBar);
                Utils.error_dialog(ShowDetailActivity.this,"Error", t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Animation.fade(ShowDetailActivity.this);
    }

}
