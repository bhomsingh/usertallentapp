package dilawarkhan.azeemi.talentuser.Activity;

import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import dilawarkhan.azeemi.talentuser.Api.ApiUtils;
import dilawarkhan.azeemi.talentuser.Api.SOService;
import dilawarkhan.azeemi.talentuser.Api.Utils;
import dilawarkhan.azeemi.talentuser.Classes.Animation;
import dilawarkhan.azeemi.talentuser.Classes.CategoryForSpinnerClass;
import dilawarkhan.azeemi.talentuser.Classes.SubCategoryForSpinnerClass;
import dilawarkhan.azeemi.talentuser.R;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SearchActivity extends AppCompatActivity{

    private Toolbar toolbar;
    private Spinner spin_category, spin_sub_category,spin_featured;
    private EditText et_keyword;
    private Button search_button;
    private List<CategoryForSpinnerClass> categoryForSpinnerClassList;
    private List<String> categoryList;
    private List<SubCategoryForSpinnerClass> subCategoryForSpinnerClassList;
    private List<String> subCategoryList;
    private String cat_id,subCat_id,fecture_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Search");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        spin_category = findViewById(R.id.spin_category);
        spin_sub_category = findViewById(R.id.spin_sub_category);
        spin_featured = findViewById(R.id.spin_featured);

        et_keyword = findViewById(R.id.et_keyword);
        search_button = findViewById(R.id.search_button);

        if (Utils.isOnline(SearchActivity.this))
        {
            // Load Category in spinner
            LoadCategoryNameListInSpinner();
        }
        else
        {
            Utils.error_dialog(SearchActivity.this, "Internet Error", "Internet Not Found");
        }

        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utils.isOnline(SearchActivity.this)) {

                    Intent intent = new Intent(SearchActivity.this, SearchFilterActivity.class);
                    intent.putExtra("cat_id", cat_id);
                    intent.putExtra("subCat_id", subCat_id);
                    String txt = et_keyword.getText().toString();
                    intent.putExtra("et_keyword", txt);
                    intent.putExtra("fect_id", fecture_id);
                    startActivity(intent);

                }else
                {

                    Utils.error_dialog(SearchActivity.this, "Internet Error", "Internet Not Found");

                }

            }
        });// close the Search Button


        final String[] items = new String[]{"Please Select","yes", "no"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        spin_featured.setAdapter(adapter);

        //Set Select listener For category spinner
        spin_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                cat_id =  categoryForSpinnerClassList.get(i).getCategoryId();
                if (Utils.isOnline(SearchActivity.this))
                {

                    LoadSubCategoryNameListInSpinner(cat_id);

                 }
                else
                {
                    Utils.error_dialog(SearchActivity.this, "Internet Error", "Internet Not Found");
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //Set Select listener For Sub category spinner
        spin_sub_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                subCat_id =  subCategoryForSpinnerClassList.get(i).getSubcategoryId();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //Set Select  Is Feature spinner
        spin_featured.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                fecture_id = String.valueOf(i);

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void LoadCategoryNameListInSpinner()
    {
        SOService soService = ApiUtils.getSOService();
        soService.getCategoryListForSpinnerResponse(ApiUtils.BASE_URL, Utils.getSimpleTextBody("load_category_for_form")).enqueue(new Callback<List<CategoryForSpinnerClass>>() {
            @Override
            public void onResponse(Call<List<CategoryForSpinnerClass>> call, Response<List<CategoryForSpinnerClass>> response) {
                if (response.isSuccessful())
                {
                    categoryForSpinnerClassList = response.body();
                    categoryList = new ArrayList<>();

                    categoryList.add(0,"Please Select");
                    if(categoryForSpinnerClassList.get(0).getSuccess())
                    {
                        for (int i = 0; i<categoryForSpinnerClassList.size(); i++)
                        {
                            categoryList.add(categoryForSpinnerClassList.get(i).getCategoryName());
                        }
                        spin_category.setAdapter(new ArrayAdapter<>(SearchActivity.this,android.R.layout.simple_spinner_dropdown_item,categoryList));
                    }
                    else
                    {
                        Toasty.error(SearchActivity.this,categoryForSpinnerClassList.get(0).getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<CategoryForSpinnerClass>> call, Throwable t) {
                Toasty.error(SearchActivity.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void LoadSubCategoryNameListInSpinner(String cat_id)
    {

        SOService soService = ApiUtils.getSOService();
        soService.getSubCategoryListForSpinnerResponse(ApiUtils.BASE_URL, Utils.getSimpleTextBody("load_Sub_category_for_form"),Utils.getSimpleTextBody(cat_id)).enqueue(new Callback<List<SubCategoryForSpinnerClass>>() {
            @Override
            public void onResponse(Call<List<SubCategoryForSpinnerClass>> call, Response<List<SubCategoryForSpinnerClass>> response) {
                if(response.isSuccessful())
                {
                    subCategoryForSpinnerClassList = response.body();
                    subCategoryList = new ArrayList<>();
                    if(subCategoryForSpinnerClassList.get(0).getSuccess())
                    {
                        for (int i = 0; i<subCategoryForSpinnerClassList.size(); i++ )
                        {
                            subCategoryList.add(subCategoryForSpinnerClassList.get(i).getSubcategoryName());
                        }
                        spin_sub_category.setAdapter(new ArrayAdapter<>(SearchActivity.this,android.R.layout.simple_spinner_dropdown_item,subCategoryList));
                    }
                    else
                    {

                        subCategoryList.clear();
                        spin_sub_category.setAdapter(new ArrayAdapter<>(SearchActivity.this,android.R.layout.simple_spinner_dropdown_item,subCategoryList));
                        Toasty.error(SearchActivity.this, subCategoryForSpinnerClassList.get(0).getMessage(), Toast.LENGTH_LONG).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<List<SubCategoryForSpinnerClass>> call, Throwable t) {
                Toasty.error(SearchActivity.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Animation.slideUp(SearchActivity.this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        Animation.slideUp(SearchActivity.this);
        return true;
    }

}