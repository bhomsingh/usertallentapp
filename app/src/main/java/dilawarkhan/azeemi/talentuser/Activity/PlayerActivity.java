package dilawarkhan.azeemi.talentuser.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.widget.MediaController;
import android.widget.TextView;

import com.bambuser.broadcaster.BroadcastPlayer;
import com.bambuser.broadcaster.PlayerState;
import com.google.gson.Gson;

import java.util.List;

import dilawarkhan.azeemi.talentuser.Api.ApiUtils;
import dilawarkhan.azeemi.talentuser.Api.SOService;
import dilawarkhan.azeemi.talentuser.Api.Utils;
import dilawarkhan.azeemi.talentuser.Classes.LiveStreamResultClass;
import dilawarkhan.azeemi.talentuser.Classes.Result;
import dilawarkhan.azeemi.talentuser.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlayerActivity extends AppCompatActivity {

    private SurfaceView mVideoSurface;
    private BroadcastPlayer mBroadcastPlayer;
    private MediaController mMediaController;
    private TextView mPlayerStatusTextView;
    private String ResourceUri, show_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        show_id = getIntent().getStringExtra("show_id");

        mVideoSurface = findViewById(R.id.VideoSurfaceView);
        mPlayerStatusTextView = findViewById(R.id.PlayerStatusTextView);
        if (Utils.isOnline(this))
        {
            LoadLiveStream();
        }
        else
        {
            Utils.error_dialog(PlayerActivity.this,"Internet Error","Internet Not Found");
        }
    }

    private void LoadLiveStream()
    {
        SOService soService = ApiUtils.getSOService();
        soService.getLiveResponse(ApiUtils.BASE_URL, Utils.getSimpleTextBody("load_live_stream_of_show"),Utils.getSimpleTextBody(show_id)).enqueue(new Callback<LiveStreamResultClass>() {
            @Override
            public void onResponse(Call<LiveStreamResultClass> call, Response<LiveStreamResultClass> response) {
                if(response.isSuccessful())
                {
                    if(response.body().getResults().size() != 0)
                    {
                        if(response.body().getResults().get(0).getType().equals("live"))
                        {
                            initPlayer(response.body().getResults().get(0).getResourceUri());
                        }
                        else
                        {
                            Utils.error_dialog(PlayerActivity.this,"Error","Streaming Not available.");
                        }
                    }
                    else
                    {
                        Utils.error_dialog(PlayerActivity.this,"Error","Streaming Not available.");
                    }
//                    initPlayer(response.body().getResults().get(0).getResourceUri());
//                    mPlayerStatusTextView.setText(response.body().getResults().get(0).getResourceUri());
                }

            }

            @Override
            public void onFailure(Call<LiveStreamResultClass> call, Throwable t) {
                Utils.error_dialog(PlayerActivity.this,"Error", t.getMessage());
            }
        });
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        mVideoSurface = null;
        if (mBroadcastPlayer != null)
            mBroadcastPlayer.close();
        mBroadcastPlayer = null;
        if (mMediaController != null)
            mMediaController.hide();
        mMediaController = null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mVideoSurface = findViewById(R.id.VideoSurfaceView);
        mPlayerStatusTextView.setText("Loading latest broadcast");
        initPlayer(ResourceUri);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (ev.getActionMasked() == MotionEvent.ACTION_UP && mBroadcastPlayer != null && mMediaController != null) {
            PlayerState state = mBroadcastPlayer.getState();
            if (state == PlayerState.PLAYING ||
                    state == PlayerState.BUFFERING ||
                    state == PlayerState.PAUSED ||
                    state == PlayerState.COMPLETED) {
                if (mMediaController.isShowing())
                    mMediaController.hide();
                else
                    mMediaController.show();
            } else {
                mMediaController.hide();
            }
        }
        return false;
    }

    void initPlayer(String resourceUri) {
        if (resourceUri == null) {
            if (mPlayerStatusTextView != null)
                mPlayerStatusTextView.setText("Could not get info about latest broadcast");
            return;
        }
        if (mVideoSurface == null) {
            // UI no longer active
            return;
        }
        if (mBroadcastPlayer != null)
            mBroadcastPlayer.close();
        mBroadcastPlayer = new BroadcastPlayer(this, resourceUri, ApiUtils.APPLICATION_ID, mBroadcastPlayerObserver);
        mBroadcastPlayer.setSurfaceView(mVideoSurface);
        mBroadcastPlayer.load();
    }

    BroadcastPlayer.Observer mBroadcastPlayerObserver = new BroadcastPlayer.Observer() {
        @Override
        public void onStateChange(PlayerState playerState) {
            if (mPlayerStatusTextView != null)
                mPlayerStatusTextView.setText("Status: " + playerState);
            if (playerState == PlayerState.PLAYING || playerState == PlayerState.PAUSED || playerState == PlayerState.COMPLETED) {
                if (mMediaController == null && mBroadcastPlayer != null && !mBroadcastPlayer.isTypeLive()) {
                    mMediaController = new MediaController(PlayerActivity.this);
                    mMediaController.setAnchorView(mVideoSurface);
                    mMediaController.setMediaPlayer(mBroadcastPlayer);
                }
                if (mMediaController != null) {
                    mMediaController.setEnabled(true);
                    mMediaController.show();
                }
            } else if (playerState == PlayerState.ERROR || playerState == PlayerState.CLOSED) {
                if (mMediaController != null) {
                    mMediaController.setEnabled(false);
                    mMediaController.hide();
                }
                mMediaController = null;
            }
        }
        @Override
        public void onBroadcastLoaded(boolean live, int width, int height) {
        }
    };
}
