package dilawarkhan.azeemi.talentuser.Activity;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.List;

import dilawarkhan.azeemi.talentuser.Adapter.ShowFeaturedOrignalAdapter;
import dilawarkhan.azeemi.talentuser.Adapter.ShowOrignalAdapter;
import dilawarkhan.azeemi.talentuser.Api.ApiUtils;
import dilawarkhan.azeemi.talentuser.Api.SOService;
import dilawarkhan.azeemi.talentuser.Api.Utils;
import dilawarkhan.azeemi.talentuser.Classes.Animation;
import dilawarkhan.azeemi.talentuser.Classes.ShowClass;
import dilawarkhan.azeemi.talentuser.R;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ProgressBar progressBar, progressBottom;
    private android.view.animation.Animation animation;
    private RecyclerView recyclerView;
    private RelativeLayout layout;
    private List<ShowClass> showClassList;
    private ShowOrignalAdapter showOriginalAdapter;
    private LinearLayoutManager manager;
    private Boolean isScrolling = false;
    private int currentItems, totalItems = 0, scrollOutItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);

        //Testing
        toolbar = findViewById(R.id.toolbar_myShow);
        toolbar.setTitle(" Shows ");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = findViewById(R.id.recycler_view);
        progressBar = findViewById(R.id.progress);
        progressBottom = findViewById(R.id.progressBottom);
        layout = findViewById(R.id.freez_layout);

        if (Utils.isOnline(ShowActivity.this)) {
            Utils.disable_layout(ShowActivity.this, animation, layout, progressBar);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        LoadShowList();
                    } catch (Exception e) {
                        Toasty.error(ShowActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, 2000);
        } else {
            Utils.error_dialog(ShowActivity.this, "Internet Error", "Internet Not Found");
        }

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = manager.getChildCount();
                totalItems = manager.getItemCount();

                scrollOutItems = manager.findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                        //Fetch new Data
                        isScrolling = false;
                        Utils.EnableProgressBar(progressBottom);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    progressBottom.setVisibility(View.GONE);
                                    LoadShowListPagination();
                                } catch (Exception e) {
                                    Toasty.error(ShowActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, 2000);
                    }
                }
            }
        });
    }// close the ONCreate Methord

    private void LoadShowList(){


        SOService soService = ApiUtils.getSOService();
        soService.getShowListResponse(ApiUtils.BASE_URL, Utils.getSimpleTextBody("load_shows_list")
                , Utils.getSimpleTextBody(String.valueOf(totalItems))).enqueue(new Callback<List<ShowClass>>() {
            @Override
            public void onResponse(Call<List<ShowClass>> call, Response<List<ShowClass>> response) {

                if(response.isSuccessful())
                {
                    Utils.enableLayout(ShowActivity.this,animation,layout,progressBar);
                    showClassList = response.body();
                    if(showClassList.get(0).getSuccess())
                    {
                        showOriginalAdapter = new ShowOrignalAdapter(ShowActivity.this,showClassList);
                        recyclerView.setAdapter(showOriginalAdapter);
                        manager = new LinearLayoutManager(ShowActivity.this);
                        manager.setOrientation(LinearLayoutManager.VERTICAL);
                        recyclerView.setLayoutManager(manager);
                    }
                    else
                    {
                        Utils.error_dialog(ShowActivity.this,"Error", showClassList.get(0).getMessage());
                    }
                }
                else
                {
                    Utils.error_dialog(ShowActivity.this,"Error", response.message());
                }
            }

            @Override
            public void onFailure(Call<List<ShowClass>> call, Throwable t) {
                Toasty.error(ShowActivity.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }



    private void  LoadShowListPagination(){

        SOService soService = ApiUtils.getSOService();
        soService.getShowListResponse(ApiUtils.BASE_URL, Utils.getSimpleTextBody("load_shows_list"),
                Utils.getSimpleTextBody(String.valueOf(totalItems))).enqueue(new Callback<List<ShowClass>>() {
            @Override
            public void onResponse(Call<List<ShowClass>> call, Response<List<ShowClass>> response) {
                if(response.isSuccessful())
                {
                    progressBottom.setVisibility(View.GONE);
                    showClassList = response.body();
                    if(showClassList.get(0).getSuccess())
                    {
                        showOriginalAdapter.addNewData(showClassList);
                    }
                    else
                    {
                        //Utils.error_dialog(CategoryActivity.this,"Error", categoryClasList.get(0).getMessage());
                    }
                }
                else
                {
                    Utils.error_dialog(ShowActivity.this,"Error", response.message());
                }
            }

            @Override
            public void onFailure(Call<List<ShowClass>> call, Throwable t) {
                progressBottom.setVisibility(View.GONE);
                Toasty.error(ShowActivity.this,t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        Animation.slideUp(ShowActivity.this);
        return true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Animation.slideUp(ShowActivity.this);
    }
}
